# YouboraLib JS
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com) [![build](https://bitbucket-badges.atlassian.io/badge/npaw/lib-plugin-js.svg)](https://bitbucket.org/npaw/lib-plugin-js/) [![codecov](https://codecov.io/bb/npaw/lib-plugin-js/branch/master/graph/badge.svg)](https://codecov.io/bb/npaw/lib-plugin-js)

## Folder Structure
```
/
├── dist/     Built & Minified files ready to include and run
├── spec/     Tests
├── samples/  Sample files
└── src/      Sourcecode of the project
```

## Documentation, Installation & Usage
Please refer to [Developer Portal](http://developer.nicepeopleatwork.com).

## I need help!
If you find a bug, have a suggestion or need assistance send an E-mail to <support@nicepeopleatwork.com>
