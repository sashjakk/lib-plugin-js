describe('ResourceTransform', () => {
  var ResourceTransform = require('../../../../src/comm/transform/resource')
  var rt

  var infoA = {
    isParseHls: function () { return true },
    isLocHeader: function () { return false },
    isParseCdnNode: function () { return false },
    isDash: function () { return false },
    getParseCdnNodeNameHeader: function () { return false },
    getParseCdnNodeList: function () { return [] }
  }
  var infoB = {
    isParseHls: function () { return false },
    isLocHeader: function () { return false },
    isParseCdnNode: function () { return true },
    isDash: function () { return false },
    getParseCdnNodeNameHeader: function () { return true },
    getParseCdnNodeList: function () { return [] }
  }
  var infoC = {
    isParseHls: function () { return false },
    isLocHeader: function () { return true },
    isParseCdnNode: function () { return false },
    isDash: function () { return false },
    getParseCdnNodeNameHeader: function () { return true },
    getParseCdnNodeList: function () { return [] }
  }

  var infoD = {
    isParseHls: function () { return false },
    isLocHeader: function () { return false },
    isParseCdnNode: function () { return false },
    isDash: function () { return true },
    getParseCdnNodeNameHeader: function () { return false },
    getParseCdnNodeList: function () { return [] }
  }

  beforeEach(() => { })

  it('should call parse HLS', () => {
    rt = new ResourceTransform(infoA)
    spyOn(rt, '_parseHLS')
    spyOn(rt, '_parseCDN')
    spyOn(rt, '_parseLocHeader')
    spyOn(rt, '_dashParse')
    spyOn(rt, '_setTimeout')
    rt.init('')
    expect(rt._parseCDN).not.toHaveBeenCalled()
    expect(rt._parseLocHeader).not.toHaveBeenCalled()
    expect(rt._dashParse).not.toHaveBeenCalled()
    expect(rt._parseHLS).toHaveBeenCalled()
    expect(rt._setTimeout).toHaveBeenCalled()
  })

  it('should call parse CDN', () => {
    rt = new ResourceTransform(infoB)
    spyOn(rt, '_parseHLS')
    spyOn(rt, '_parseCDN')
    spyOn(rt, '_parseLocHeader')
    spyOn(rt, '_dashParse')
    spyOn(rt, '_setTimeout')
    rt.init('')
    expect(rt._parseCDN).toHaveBeenCalled()
    expect(rt._parseLocHeader).not.toHaveBeenCalled()
    expect(rt._dashParse).not.toHaveBeenCalled()
    expect(rt._parseHLS).not.toHaveBeenCalled()
    expect(rt._setTimeout).toHaveBeenCalled()
  })

  it('should call parse Location header', () => {
    rt = new ResourceTransform(infoC)
    spyOn(rt, '_parseHLS')
    spyOn(rt, '_parseCDN')
    spyOn(rt, '_parseLocHeader')
    spyOn(rt, '_dashParse')
    spyOn(rt, '_setTimeout')
    rt.init('')
    expect(rt._parseCDN).not.toHaveBeenCalled()
    expect(rt._parseLocHeader).toHaveBeenCalled()
    expect(rt._dashParse).not.toHaveBeenCalled()
    expect(rt._parseHLS).not.toHaveBeenCalled()
    expect(rt._setTimeout).toHaveBeenCalled()
  })

  it('should call parse Dash', () => {
    rt = new ResourceTransform(infoD)
    spyOn(rt, '_parseHLS')
    spyOn(rt, '_parseCDN')
    spyOn(rt, '_parseLocHeader')
    spyOn(rt, '_dashParse')
    spyOn(rt, '_setTimeout')
    rt.init('')
    expect(rt._parseCDN).not.toHaveBeenCalled()
    expect(rt._parseLocHeader).not.toHaveBeenCalled()
    expect(rt._dashParse).toHaveBeenCalled()
    expect(rt._parseHLS).not.toHaveBeenCalled()
    expect(rt._setTimeout).toHaveBeenCalled()
  })

  it('should return the resource', () => {
    rt = new ResourceTransform(infoA)
    rt._initResource = 'asd'
    expect(rt.getResource()).toBe(null)
    rt._realResource = 'real'
    expect(rt.getResource()).toBe('real')
  })
})
