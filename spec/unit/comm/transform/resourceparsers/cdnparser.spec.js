describe('CdnParser', () => {
  var CdnParser = require('../../../../../src/comm/transform/resourceparsers/cdnparser')

  it('should parse HIGHWINDS', () => {
    var t = CdnParser.create('Highwinds')
    t.parse('url', {
      '{}': 'X-HW: 1479307166.dop017.fr7.t,1479307165.cds061.fr7.p'
    })
    expect(t.getParsedCdnName()).toBe('HIGHNEGR')
    expect(t.getParsedNodeHost()).toBe('cds061.fr7')
    expect(t.getParsedNodeTypeString()).toBe('p')
    expect(t.getParsedNodeType()).toBe(2)

    t.parse('url', {
      '{}': 'X-HW: 1479307166.dop017.fr7.t,1479307165.cds061.fr7.x'
    })
    expect(t.getParsedNodeTypeString()).toBe('x')
    expect(t.getParsedNodeType()).toBe(1)
    t.parse('url', {
      '{}': 'X-HW: 1479307166.dop017.fr7.t,1479307165.cds061.fr7.c'
    })
    expect(t.getParsedNodeTypeString()).toBe('c')
    expect(t.getParsedNodeType()).toBe(1)
  })

  it('should parse Level3', () => {
    var t = CdnParser.create('Level3')
    t.parse('url', {
      '{"X-WR-Diag":"host"}': 'X-WR-Diag: Host:251223/a Type:TCP_MEM_HIT'
    })
    expect(t.getParsedCdnName()).toBe('LEVEL3')
    expect(t.getParsedNodeHost()).toBe('251223/a')
    expect(t.getParsedNodeTypeString()).toBe('TCP_MEM_HIT')
    expect(t.getParsedNodeType()).toBe(1)

    t.parse('url', {
      '{"X-WR-Diag":"host"}': 'X-WR-Diag: Host:251223/a Type:TCP_MISS'
    })
    expect(t.getParsedNodeTypeString()).toBe('TCP_MISS')
    expect(t.getParsedNodeType()).toBe(2)

    t.parse('url', {
      '{"X-WR-Diag":"host"}': 'X-WR-Diag: Host:251223/a Type:?'
    })
    expect(t.getParsedNodeTypeString()).toBe('?')
    expect(t.getParsedNodeType()).toBe(0)
  })

  it('should parse CLOUDFRONT', () => {
    var t = CdnParser.create('Cloudfront')
    t.parse('url', {
      '{}': 'X-Cache: Hit from s3\n' +
                'X-Amz-Cf-Id: S1449163321'
    })
    expect(t.getParsedCdnName()).toBe('CLOUDFRT')
    expect(t.getParsedNodeHost()).toBe('S1449163321')
    expect(t.getParsedNodeTypeString()).toBe('Hit')
    expect(t.getParsedNodeType()).toBe(1)

    t.parse('url', {
      '{}': 'X-Cache: Miss from s3\n' +
                'X-Amz-Cf-Id: S1449163321'
    })
    expect(t.getParsedNodeTypeString()).toBe('Miss')
    expect(t.getParsedNodeType()).toBe(2)

    t.parse('url', {
      '{}': 'X-Cache: ? from s3\n' +
                'X-Amz-Cf-Id: S1449163321'
    })
    expect(t.getParsedNodeTypeString()).toBe('?')
    expect(t.getParsedNodeType()).toBe(0)
  })

  it('should parse AKAMAI', () => {
    var t = CdnParser.create('Akamai')
    t._options.requestHeaders = {}
    t.parse('url', {
      '{}': 'X-Cache: TCP_REFRESH_MISS from a104-90-205-28.deploy.akamaitechnologies.com (AkamaiGHost/9.6.4.1-25700704) (S)'
    })
    expect(t.getParsedCdnName()).toBe('AKAMAI')
    expect(t.getParsedNodeHost()).toBe('9.6.4.1-25700704')
    expect(t.getParsedNodeTypeString()).toBe('TCP_REFRESH_MISS')
    expect(t.getParsedNodeType()).toBe(2)

    t.parse('url', {
      '{}': 'X-Cache: TCP_HIT from a104-90-205-28.deploy.akamaitechnologies.com (AkamaiGHost/9.6.4.1-25700704) (S)'
    })
    expect(t.getParsedCdnName()).toBe('AKAMAI')
    expect(t.getParsedNodeHost()).toBe('9.6.4.1-25700704')
    expect(t.getParsedNodeTypeString()).toBe('TCP_HIT')
    expect(t.getParsedNodeType()).toBe(1)

    t.parse('url', {
      '{}': 'X-Cache: ? from a104-90-205-28.deploy.akamaitechnologies.com (AkamaiGHost/9.6.4.1-25700704) (S)'
    })
    expect(t.getParsedCdnName()).toBe('AKAMAI')
    expect(t.getParsedNodeHost()).toBe('9.6.4.1-25700704')
    expect(t.getParsedNodeTypeString()).toBe('?')
    expect(t.getParsedNodeType()).toBe(0)
  })

  it('should parse FASTLY', () => {
    var t = CdnParser.create('Fastly')
    t.parse('url', {
      '{}': 'X-Served-By: cache-ams4130-AMS\n' +
                'X-Cache: MISS'
    })
    expect(t.getParsedCdnName()).toBe('FASTLY')
    expect(t.getParsedNodeHost()).toBe('cache-ams4130-AMS')
    expect(t.getParsedNodeTypeString()).toBe('MISS')
    expect(t.getParsedNodeType()).toBe(2)

    t.parse('url', {
      '{}': 'X-Served-By: cache-ams4130-AMS\n' +
                'X-Cache: HIT'
    })
    expect(t.getParsedNodeTypeString()).toBe('HIT')
    expect(t.getParsedNodeType()).toBe(1)

    t.parse('url', {
      '{}': 'X-Served-By: cache-ams4130-AMS\n' +
                'X-Cache: ?'
    })
    expect(t.getParsedNodeTypeString()).toBe('?')
    expect(t.getParsedNodeType()).toBe(0)
  })

  it('should parse Telefonica', () => {
    var t = CdnParser.create('Telefonica')
    t.parse('url', {
      '{"x-tcdn":"host"}': 'x-tcdn: Host:251223 Type:p'
    })
    expect(t.getParsedCdnName()).toBe('TELEFO')
    expect(t.getParsedNodeHost()).toBe('251223')
    expect(t.getParsedNodeTypeString()).toBe('p')
    expect(t.getParsedNodeType()).toBe(1)

    t.parse('url', {
      '{"x-tcdn":"host"}': 'x-tcdn: Host:251223 Type:m'
    })
    expect(t.getParsedNodeTypeString()).toBe('m')
    expect(t.getParsedNodeType()).toBe(2)

    t.parse('url', {
      '{"x-tcdn":"host"}': 'x-tcdn: Host:251223 Type:?'
    })
    expect(t.getParsedNodeTypeString()).toBe('?')
    expect(t.getParsedNodeType()).toBe(0)
  })

  it('should parse BALANCER', () => {
    var t = CdnParser.create('Balancer')
    CdnParser.setBalancerHeaderName('x-cdn-forward')
    t.parse('url', {
      '{}': 'x-cdn-forward: CUSTOM'
    })
    expect(t.getParsedCdnName()).toBe('CUSTOM')
  })

  it('should build a CdnParser', () => {
    var t = new CdnParser()
    t.addParser({
      element: CdnParser.ElementType.HOST_AND_TYPE,
      headerName: 'X-WR-Diag',
      regex: /Host:(.+)\sType:(.+)/
    })
    t.setCdnName('LEVEL3')
    t.setRequestHeader('X-WR-Diag', 'host')
    t.setParseType(function (type) {
      switch (type) {
        case 'TCP_HIT':
        case 'TCP_MEM_HIT':
        case 'TCP_IMS_HIT':
          return 1
        case 'TCP_MISS':
          return 2
        default:
          return 0
      }
    })

    t.parse('url', {
      '{"X-WR-Diag":"host"}': 'X-WR-Diag: Host:251223/a Type:TCP_MEM_HIT'
    })
    expect(t.getParsedCdnName()).toBe('LEVEL3')
    expect(t.getParsedNodeHost()).toBe('251223/a')
    expect(t.getParsedNodeTypeString()).toBe('TCP_MEM_HIT')
    expect(t.getParsedNodeType()).toBe(1)
  })
})
