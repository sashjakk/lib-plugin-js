/* eslint no-extend-native: "off", no-extra-bind: "off" */

describe('Polyfiller', () => {
  var objectCreate = require('../../src/mixins/create')

  beforeEach(() => {
    Function.prototype.bind = undefined
    Array.prototype.forEach = undefined
    require('../../src/polyfills')()
  })

  it('Function.prototype.bind', (done) => {
    setTimeout(function () {
      this()
    }.bind(done))
  })

  it('Array.forEach', () => {
    var arr = [0]
    arr.forEach((v) => {
      expect(v).toBe(0)
    })
  })

  it('Array.forEach should throw exception if not function', () => {
    expect(function () { [].forEach('not-a-function') }).toThrow()
  })

  it('Object.create should throw', () => {
    expect(() => { objectCreate(1, 2) }).toThrow()
    expect(() => { objectCreate(null) }).toThrow()
    expect(() => { objectCreate(1) }).toThrow()
  })
})
