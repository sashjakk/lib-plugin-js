describe('Util', () => {
  var Util = require('../../src/util.js')

  it('should strip protocols', () => {
    expect(Util.stripProtocol('http://google.com')).toBe('google.com')
    expect(Util.stripProtocol('https://google.com')).toBe('google.com')
    expect(Util.stripProtocol('//google.com')).toBe('google.com')
    expect(JSON.stringify(Util.stripProtocol({}))).toBe('{}')
  })

  it('should add protocol', () => {
    expect(Util.addProtocol('google.com', true)).toBe('https://google.com')
    expect(Util.addProtocol('google.com')).toBe('http://google.com')
    expect(Util.addProtocol('google.com', false)).toBe('http://google.com')
  })

  it('should parse numbers', () => {
    expect(Util.parseNumber(NaN, 0)).toBe(0)
    expect(Util.parseNumber(-1, 1)).toBe(1)
    expect(Util.parseNumber(Infinity, 2)).toBe(2)
    expect(Util.parseNumber(null, 3)).toBe(3)
    expect(Util.parseNumber(undefined, 4)).toBe(4)
  })

  it('should build rendition strings', () => {
    expect(Util.buildRenditionString(100, 100, -100)).toBe('100x100')
    expect(Util.buildRenditionString(100, 100, 1000000)).toBe('100x100@1Mbps')
    expect(Util.buildRenditionString(100)).toBe('100bps')
    expect(Util.buildRenditionString(1000)).toBe('1Kbps')
  })

  describe('listen all events', () => {
    it('should work with addEventListener', () => {
      var object = {
        addEventListener: () => { }
      }
      spyOn(object, 'addEventListener')

      Util.logAllEvents(object)
      expect(object.addEventListener).toHaveBeenCalled()
    })

    it('should work with on', () => {
      var object = {
        on: () => { }
      }
      spyOn(object, 'on')

      Util.logAllEvents(object)
      expect(object.on).toHaveBeenCalled()
    })

    it('should work with custom function', () => {
      var object = {
        custom: () => { }
      }
      spyOn(object, 'custom')

      Util.logAllEvents(object.custom)
      expect(object.custom).toHaveBeenCalled()
    })

    it('should work with extra events and custom report', () => {
      var object = {
        addEventListener: () => { }
      }
      spyOn(object, 'addEventListener')

      Util.logAllEvents(object, [null, 'extraevent'], () => { })
      expect(object.addEventListener).toHaveBeenCalled()
    })

    it('Object.assign', () => {
      var obj = Util.assign({}, { a: 'b' })
      expect(obj.a).toBe('b')
    })

    it('Array.isArray', () => {
      expect(Util.isArray([])).toBe(true)
      expect(Util.isArray({})).toBe(false)
    })
  })
})
