describe('Chrono', () => {
  var Chrono = require('../../src/chrono.js')
  var chrono

  beforeEach(() => {
    chrono = new Chrono()
  })

  it('should start', () => {
    chrono.start()
    expect(chrono.startTime).toBeGreaterThan(-1)
    expect(chrono.stopTime).toBe(0)
  })

  it('should stop', () => {
    chrono.start()
    expect(chrono.stop()).toBeGreaterThan(-1)
    expect(chrono.getDeltaTime()).toBeGreaterThan(-1)
  })

  it('should stop with getDeltaTime(true)', () => {
    chrono.start()
    expect(chrono.getDeltaTime(true)).toBeGreaterThan(-1)
    expect(chrono.getDeltaTime()).toBeGreaterThan(-1)
  })

  it('should work if stop is called before start', () => {
    expect(chrono.stop()).toBe(-1)
    expect(chrono.getDeltaTime()).toBe(-1)
  })

  it('should work', (done) => {
    chrono.start()
    setTimeout(() => {
      expect(chrono.stop() / 1000).toBeCloseTo(1, 0)
      expect(chrono.getDeltaTime() / 1000).toBeCloseTo(1, 1)
      done()
    }, 1000)
  })

  it('should pause and resume the chronos', () => {
    chrono.start()
    chrono.pause()
    expect(chrono.pauseTime).not.toBeFalsy()
    chrono.resume()
    expect(chrono.pauseTime).toBeFalsy()
    expect(chrono.offset).toBeDefined()
  })
})
