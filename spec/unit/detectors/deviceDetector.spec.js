describe('Device detector', () => {
  var DeviceDetector = require('../../../src/detectors/deviceDetector')
  var desktopUserAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36'
  var androidUserAgent = 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Mobile Safari/537.36'
  var iosUserAgent = 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) CriOS/56.0.2924.75 Mobile/14E5239e Safari/602.1'
  var tvUserAgent = 'Opera/9.80 (Linux armv7l; HbbTV/1.2.1 (; Philips; 40HFL5010T12; ; PHILIPSTV; CE-HTML/1.0 NETTV/4.4.1 SmartTvA/3.0.0 Firmware/004.002.036.135 (PhilipsTV, 3.1.1,)en) ) Presto/2.12.407 Version/12.50'
  var playstationAgent = 'Mozilla/5.0 (PlayStation 4) AppleWebKit/531.3 (KHTML, like Gecko) SCEE/1.0 Nuanti/2.0'

  it('should not work, without navigator', () => {
    var detector = new DeviceDetector()
    expect(detector.isIphone()).not.toBeDefined()
    expect(detector.isAndroid()).not.toBeDefined()
    expect(detector.isSmartphone()).not.toBeDefined()
    expect(detector.isDesktop()).not.toBeDefined()
    expect(detector.isSmartTV()).not.toBeDefined()
    expect(detector.isPlayStation()).not.toBeDefined()
  })

  it('should not work, without user agent', () => {
    navigator = {}
    var detector = new DeviceDetector()
    expect(detector.isIphone()).not.toBeDefined()
    expect(detector.isAndroid()).not.toBeDefined()
    expect(detector.isSmartphone()).not.toBeDefined()
    expect(detector.isDesktop()).not.toBeDefined()
    expect(detector.isSmartTV()).not.toBeDefined()
    expect(detector.isPlayStation()).not.toBeDefined()
  })

  it('should be constructed, and desktop be the default value', () => {
    navigator = {
      userAgent: 'invalid value'
    }
    var detector = new DeviceDetector()
    expect(detector.isIphone()).toBeDefined()
    expect(detector.isAndroid()).toBeDefined()
    expect(detector.isSmartphone()).toBeDefined()
    expect(detector.isDesktop()).toBeDefined()
    expect(detector.isDesktop()).toBeTruthy()
    expect(detector.isSmartTV()).toBeDefined()
    expect(detector.isPlayStation()).toBeDefined()
  })

  it('should detect desktop devices', () => {
    navigator = {
      userAgent: desktopUserAgent
    }
    var detector = new DeviceDetector()
    expect(detector.isIphone()).toBeFalsy()
    expect(detector.isAndroid()).toBeFalsy()
    expect(detector.isSmartphone()).toBeFalsy()
    expect(detector.isDesktop()).toBeTruthy()
    expect(detector.isSmartTV()).toBeFalsy()
    expect(detector.isPlayStation()).toBeFalsy()
  })

  it('should detect android devices', () => {
    navigator = {
      userAgent: androidUserAgent
    }
    var detector = new DeviceDetector()
    expect(detector.isIphone()).toBeFalsy()
    expect(detector.isAndroid()).toBeTruthy()
    expect(detector.isSmartphone()).toBeTruthy()
    expect(detector.isDesktop()).toBeFalsy()
    expect(detector.isSmartTV()).toBeFalsy()
    expect(detector.isPlayStation()).toBeFalsy()
  })

  it('should detect iphonish devices', () => {
    navigator = {
      userAgent: iosUserAgent
    }
    var detector = new DeviceDetector()
    expect(detector.isIphone()).toBeTruthy()
    expect(detector.isAndroid()).toBeFalsy()
    expect(detector.isSmartphone()).toBeTruthy()
    expect(detector.isDesktop()).toBeFalsy()
    expect(detector.isSmartTV()).toBeFalsy()
    expect(detector.isPlayStation()).toBeFalsy()
  })

  it('should detect smarttv devices', () => {
    navigator = {
      userAgent: tvUserAgent
    }
    var detector = new DeviceDetector()
    expect(detector.isIphone()).toBeFalsy()
    expect(detector.isAndroid()).toBeFalsy()
    expect(detector.isSmartphone()).toBeFalsy()
    expect(detector.isDesktop()).toBeFalsy()
    expect(detector.isSmartTV()).toBeTruthy()
    expect(detector.isPlayStation()).toBeFalsy()
  })

  it('should detect playstation devices', () => {
    navigator = {
      userAgent: playstationAgent
    }
    var detector = new DeviceDetector()
    expect(detector.isIphone()).toBeFalsy()
    expect(detector.isAndroid()).toBeFalsy()
    expect(detector.isSmartphone()).toBeFalsy()
    expect(detector.isDesktop()).toBeFalsy()
    expect(detector.isSmartTV()).toBeFalsy()
    expect(detector.isPlayStation()).toBeTruthy()
  })
})
