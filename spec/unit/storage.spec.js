describe('Storage', () => {
  var YStorage = require('../../src/plugin/storage')
  var Log = require('../../src/log')
  var storage

  beforeEach(() => {
    storage = new YStorage()
    spyOn(Log, 'report')
  })

  it('should localstorage', () => {
    storage.setLocal('a', 'c')
    expect(storage.getLocal('a')).toBe('c')
  })

  it('should sessionstorage', () => {
    storage.setSession('b', 'd')
    expect(storage.getSession('b')).toBe('d')
  })

  it('should not fail if storage does not exist: using cookies', () => {
    global.sessionStorage = global.localStorage = undefined
    storage.setLocal('key', 'value')
    var val1 = storage.getLocal('key')
    storage.setSession('key', 'value2')
    var val2 = storage.getSession('key')
    expect(Log.report).not.toHaveBeenCalled()
    expect(val1).toBe('value')
    expect(val2).toBe('value2')
  })

  it('should remove cookies', () => {
    global.sessionStorage = global.localStorage = undefined
    storage.setLocal('key', 'value')
    storage.removeLocal('key')
    var val1 = storage.getLocal('key')
    storage.setSession('key', 'value2')
    storage.removeSession('key')
    var val2 = storage.getSession('key')
    expect(val1).toBeFalsy()
    expect(val2).toBeFalsy()
  })
})
