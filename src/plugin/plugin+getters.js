var Util = require('../util')

var version = require('../version')

// This file is designed to add extra functionalities to Plugin class

var PluginGetterMixin = {
  /**
     * Returns service host
     *
     * @memberof youbora.Plugin.prototype
     */
  getHost: function () {
    var host = this.options.host
    if (this.viewTransform && this.viewTransform.response && this.viewTransform.response.host) {
      host = this.viewTransform.response.host
    }
    return Util.addProtocol(Util.stripProtocol(host), this.options['app.https'])
  },

  getUserType: function () {
    return this.options['user.type']
  },

  /**
     * Returns parse HLS Flag
     *
     * @memberof youbora.Plugin.prototype
     */
  isParseHls: function () {
    return this.options['parse.hls']
  },

  /**
     * Returns parse CdnNode Flag
     *
     * @memberof youbora.Plugin.prototype
     */
  isParseCdnNode: function () {
    return this.options['parse.cdnNode']
  },

  /**
     * Returns parse location header
     *
     * @memberof youbora.Plugin.prototype
     */
  isLocHeader: function () {
    return this.options['parse.locationHeader']
  },

  /**
     * Returns parse dash
     *
     * @memberof youbora.Plugin.prototype
     */
  isDash: function () {
    return this.options['parse.dash']
  },

  /**
     * Returns Cdn list
     *
     * @memberof youbora.Plugin.prototype
     */
  getParseCdnNodeList: function () {
    return this.options['parse.cdnNode.list']
  },

  /**
     * Returns Cdn header name
     *
     * @memberof youbora.Plugin.prototype
     */
  getParseCdnNodeNameHeader: function () {
    return this.options['parse.cdnNameHeader']
  },

  /**
     * Returns obfuscateIp option
     *
     * @memberof youbora.Plugin.prototype
     */
  getObfuscateIp: function () {
    return this.options['user.obfuscateIp']
  },

  /**
     * Returns content's Extraparam1
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam1: function () {
    return this.options['content.customDimension.1']
  },

  /**
     * Returns content's Extraparam2
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam2: function () {
    return this.options['content.customDimension.2']
  },

  /**
     * Returns content's Extraparam3
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam3: function () {
    return this.options['content.customDimension.3']
  },

  /**
     * Returns content's Extraparam4
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam4: function () {
    return this.options['content.customDimension.4']
  },
  /**
     * Returns content's Extraparam5
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam5: function () {
    return this.options['content.customDimension.5']
  },

  /**
     * Returns content's Extraparam6
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam6: function () {
    return this.options['content.customDimension.6']
  },

  /**
     * Returns content's Extraparam7
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam7: function () {
    return this.options['content.customDimension.7']
  },

  /**
     * Returns content's Extraparam8
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam8: function () {
    return this.options['content.customDimension.8']
  },

  /**
     * Returns content's Extraparam9
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam9: function () {
    return this.options['content.customDimension.9']
  },

  /**
     * Returns content's Extraparam10
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam10: function () {
    return this.options['content.customDimension.10']
  },

  /**
     * Returns content's Extraparam11
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam11: function () {
    return this.options['content.customDimension.11']
  },

  /**
     * Returns content's Extraparam12
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam12: function () {
    return this.options['content.customDimension.12']
  },

  /**
     * Returns content's Extraparam13
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam13: function () {
    return this.options['content.customDimension.13']
  },

  /**
     * Returns content's Extraparam14
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam14: function () {
    return this.options['content.customDimension.14']
  },
  /**
     * Returns content's Extraparam15
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam15: function () {
    return this.options['content.customDimension.15']
  },

  /**
     * Returns content's Extraparam16
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam16: function () {
    return this.options['content.customDimension.16']
  },

  /**
     * Returns content's Extraparam17
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam17: function () {
    return this.options['content.customDimension.17']
  },

  /**
     * Returns content's Extraparam18
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam18: function () {
    return this.options['content.customDimension.18']
  },

  /**
     * Returns content's Extraparam19
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam19: function () {
    return this.options['content.customDimension.19']
  },

  /**
     * Returns content's Extraparam20
     *
     * @memberof youbora.Plugin.prototype
     */
  getExtraparam20: function () {
    return this.options['content.customDimension.20']
  },

  /**
     * Returns ad's Extraparam1
     *
     * @memberof youbora.Plugin.prototype
     */
  getAdExtraparam1: function () {
    return this.options['ad.customDimension.1']
  },

  /**
     * Returns ad's Extraparam2
     *
     * @memberof youbora.Plugin.prototype
     */
  getAdExtraparam2: function () {
    return this.options['ad.customDimension.2']
  },

  /**
     * Returns ad's Extraparam3
     *
     * @memberof youbora.Plugin.prototype
     */
  getAdExtraparam3: function () {
    return this.options['ad.customDimension.3']
  },

  /**
     * Returns ad's Extraparam4
     *
     * @memberof youbora.Plugin.prototype
     */
  getAdExtraparam4: function () {
    return this.options['ad.customDimension.4']
  },
  /**
     * Returns ad's Extraparam5
     *
     * @memberof youbora.Plugin.prototype
     */
  getAdExtraparam5: function () {
    return this.options['ad.customDimension.5']
  },

  /**
     * Returns ad's Extraparam6
     *
     * @memberof youbora.Plugin.prototype
     */
  getAdExtraparam6: function () {
    return this.options['ad.customDimension.6']
  },

  /**
     * Returns ad's Extraparam7
     *
     * @memberof youbora.Plugin.prototype
     */
  getAdExtraparam7: function () {
    return this.options['ad.customDimension.7']
  },

  /**
     * Returns ad's Extraparam8
     *
     * @memberof youbora.Plugin.prototype
     */
  getAdExtraparam8: function () {
    return this.options['ad.customDimension.8']
  },

  /**
     * Returns ad's Extraparam9
     *
     * @memberof youbora.Plugin.prototype
     */
  getAdExtraparam9: function () {
    return this.options['ad.customDimension.9']
  },

  /**
     * Returns ad's Extraparam10
     *
     * @memberof youbora.Plugin.prototype
     */
  getAdExtraparam10: function () {
    return this.options['ad.customDimension.10']
  },

  /**
     * Returns PluginInfo
     *
     * @memberof youbora.Plugin.prototype
     */
  getPluginInfo: function () {
    var ret = {
      lib: version,
      adapter: this.getAdapterVersion(),
      adAdapter: this.getAdAdapterVersion()
    }
    return ret
  },

  /**
     * Returns Ip
     *
     * @memberof youbora.Plugin.prototype
     */
  getIp: function () {
    return this.options['network.ip']
  },

  /**
     * Returns Isp
     *
     * @memberof youbora.Plugin.prototype
     */
  getIsp: function () {
    return this.options['network.isp']
  },

  /**
     * Returns ConnectionType
     *
     * @memberof youbora.Plugin.prototype
     */
  getConnectionType: function () {
    return this.options['network.connectionType']
  },

  /** Returns deviceInfo json
     *
     * @memberof youbora.Plugin.prototype
     */
  getDeviceInfo: function () {
    var info = {}
    if (this.getDeviceCode()) info.deviceCode = this.getDeviceCode()
    if (this.getModel()) info.model = this.getModel()
    if (this.getBrand()) info.brand = this.getBrand()
    if (this.getDeviceType()) info.deviceType = this.getDeviceType()
    if (this.getDeviceName()) info.deviceName = this.getDeviceName()
    if (this.getOsName()) info.osName = this.getOsName()
    if (this.getOsVersion()) info.osVersion = this.getOsVersion()
    if (this.getBrowserName()) info.browserName = this.getBrowserName()
    if (this.getBrowserVersion()) info.browserVersion = this.getBrowserVersion()
    if (this.getBrowserType()) info.browserType = this.getBrowserType()
    if (this.getBrowserEngine()) info.browserEngine = this.getBrowserEngine()
    return (info !== {} ? info : null)
  },

  /**
     * Returns DeviceCode
     *
     * @memberof youbora.Plugin.prototype
     */
  getDeviceCode: function () {
    return this.options['device.code']
  },

  /**
     * Returns Device model
     *
     * @memberof youbora.Plugin.prototype
     */
  getModel: function () {
    return this.options['device.model']
  },

  /**
     * Returns Device brand
     *
     * @memberof youbora.Plugin.prototype
     */
  getBrand: function () {
    return this.options['device.brand']
  },

  /**
     * Returns Device type
     *
     * @memberof youbora.Plugin.prototype
     */
  getDeviceType: function () {
    return this.options['device.type']
  },

  /**
     * Returns Device name
     *
     * @memberof youbora.Plugin.prototype
     */
  getDeviceName: function () {
    return this.options['device.name']
  },

  /**
     * Returns Device OS name
     *
     * @memberof youbora.Plugin.prototype
     */
  getOsName: function () {
    return this.options['device.osName']
  },

  /**
     * Returns Device OS Version
     *
     * @memberof youbora.Plugin.prototype
     */
  getOsVersion: function () {
    return this.options['device.osVersion']
  },

  /**
     * Returns Device browser name
     *
     * @memberof youbora.Plugin.prototype
     */
  getBrowserName: function () {
    return this.options['device.browserName']
  },

  /**
     * Returns Device browser version
     *
     * @memberof youbora.Plugin.prototype
     */
  getBrowserVersion: function () {
    return this.options['device.browserVersion']
  },

  /**
     * Returns Device browser type
     *
     * @memberof youbora.Plugin.prototype
     */
  getBrowserType: function () {
    return this.options['device.browserType']
  },

  /**
     * Returns DeviceCode
     *
     * @memberof youbora.Plugin.prototype
     */
  getBrowserEngine: function () {
    return this.options['device.browserEngine']
  },

  /**
     * Returns AccountCode
     *
     * @memberof youbora.Plugin.prototype
     */
  getAccountCode: function () {
    return this.options.accountCode
  },

  /**
     * Returns Username
     *
     * @memberof youbora.Plugin.prototype
     */
  getUsername: function () {
    return this.options['user.name']
  },

  /**
     * Returns AnonymousUser
     *
     * @memberof youbora.Plugin.prototype
     */
  getAnonymousUser: function () {
    return this.options['user.anonymousId']
  },

  /**
     * Returns user email
     *
     * @memberof youbora.Plugin.prototype
     */
  getEmail: function () {
    return this.options['user.email']
  },

  /**
     * Get URL referer.
     *
     * @memberof youbora.Plugin.prototype
     */
  getReferer: function () {
    if (this.options.referer) return this.options.referer
    var ret = ''
    if (typeof window !== 'undefined' && window.location) {
      ret = window.location.href
    }
    return ret
  },

  /**
     * Get URL referral.
     *
     * @memberof youbora.Plugin.prototype
     */
  getReferral: function () {
    if (typeof window !== 'undefined' && window.document) {
      return window.document.referrer
    }
    return null
  },

  /**
     * Get Browser language
     *
     * @memberof youbora.Plugin.prototype
     */
  getLanguage: function () {
    if (typeof navigator !== 'undefined') {
      return navigator.language
    }
    return null
  },

  /**
     * Returns the nodehost
     *
     * @memberof youbora.Plugin.prototype
     */
  getNodeHost: function () {
    return this.options['content.cdnNode'] || this.resourceTransform.getNodeHost()
  },

  /**
     * Returns the node type id
     *
     * @memberof youbora.Plugin.prototype
     */
  getNodeType: function () {
    return this.options['content.cdnType'] || this.resourceTransform.getNodeType()
  },

  /**
     * Returns the node type string
     *
     * @memberof youbora.Plugin.prototype
     */
  getNodeTypeString: function () {
    return this.resourceTransform.getNodeTypeString()
  },

  /**
     * Returns requestNumber value, to prevent /data calls being cached
     *
     * @memberof youbora.Plugin.prototype
     */
  getRequestNumber: function () {
    return Math.random()
  },

  /**
     * Returns a whole offline view and its id.
     *
     * @memberof youbora.Plugin.prototype
     */
  getOfflineView: function () {
    return this.offlineStorage.getView()
  },

  getDeviceUUID: function () {
    var id = null
    if (!this.options['device.isAnonymous']) {
      id = this.uuidGenerator.getKey()
    }
    return id
  },

  getSmartswitchConfigCode: function () {
    return this.options['smartswitch.configCode']
  },

  getSmartswitchGroupCode: function () {
    return this.options['smartswitch.groupCode']
  },

  getSmartswitchContractCode: function () {
    return this.options['smartswitch.contractCode']
  },

  getAppName: function () {
    return this.options['app.name']
  },

  getAppReleaseVersion: function () {
    return this.options['app.releaseVersion']
  },

  getIsBlocked: function () {
    return this.blockDetector ? this.blockDetector.isBlocked : null
  }
}

module.exports = PluginGetterMixin
