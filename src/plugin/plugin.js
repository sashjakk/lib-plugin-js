var Emitter = require('../emitter')
var Timer = require('../timer')
var Chrono = require('../chrono')
var Constants = require('../constants')
var Util = require('../util')

var YBRequest = require('../comm/request')
var Communication = require('../comm/communication')
var FlowTransform = require('../comm/transform/flow')
var ViewTransform = require('../comm/transform/view')
var ResourceTransform = require('../comm/transform/resource')
var OfflineTransform = require('../comm/transform/offline')

var Options = require('./options')
var YouboraStorage = require('./storage')
var OfflineStorage = require('./offlineStorage')
var RequestBuilder = require('./requestbuilder')

var YouboraInfinity = require('../infinity/infinity')

var HybridNetwork = require('../monitors/hybridnetwork')
var BackgroundDetector = require('../detectors/backgroundDetector')
var DeviceDetector = require('../detectors/deviceDetector')
var BlockDetector = require('../detectors/blockDetector')
var ResizeScrollDetector = require('../detectors/resizeScrollDetector')
var UUIDGenerator = require('../deviceUUID/hashgenerator')

var Plugin = Emitter.extend(
  /** @lends youbora.Plugin.prototype */
  {
    /**
     * This is the main class of video analytics. You may want to have one instance for each video
     * you want to track. Will need {@link Adapter}s for both content and ads.
     *
     * @constructs Plugin
     * @extends youbora.Emitter
     * @memberof youbora
     *
     * @param {Options} [options] An object complying with {@link Options} constructor.
     * @param {Adapter} [adapter] If an adapter is provided, setAdapter will be immediately called.
     */
    constructor: function (options, adapter) {
      /** Reference to {@link youbora.YouboraStorage} */
      this.storage = new YouboraStorage()

      /** UUIDGenerator manager */
      this.uuidGenerator = new UUIDGenerator(this)

      /** Instance of youbora infinity. */
      this.infinity = new YouboraInfinity(this)

      /** This flags indicates that /init has been called. */
      this.isInitiated = false

      /** This flags indicates that /adManifest has been called. */
      this.isAdsManifestSent = false

      /** Postroll counter to fix plugins reporting stop before postrolls */
      this.playedPostrolls = 0

      /** This flags indicates if an ad break is started */
      this.isBreakStarted = false

      /** Chrono for init times. */
      this.initChrono = new Chrono()

      /** Stored {@link Options} of the session. */
      this.options = new Options(options)

      /** Reference to {@link youbora.YouboraStorage} */
      this.storage = new YouboraStorage(null, this.options.disableCookies)

      /** Reference to {@link youbora.OfflineStorage} */
      this.offlineStorage = new OfflineStorage()

      this._adapter = null
      this._adsAdapter = null
      this._ping = new Timer(this._sendPing.bind(this), 5000)
      this._beat = new Timer(this._sendBeat.bind(this), 30000)
      this._refreshData = new Timer(this._checkOldData.bind(this), 3600000) // 1h
      this._refreshData.start()
      this.sessionExpire = Number(this.storage.getLocal('sessionExpire')) * 1000 || 300000

      this.requestBuilder = new RequestBuilder(this)

      this.resourceTransform = new ResourceTransform(this)

      this.lastEventTime = null

      if (adapter) this.setAdapter(adapter)

      this.restartViewTransform()

      this._initInfinity()

      this.hybridNetwork = new HybridNetwork()
      this.deviceDetector = new DeviceDetector()
      this.backgroundDetector = new BackgroundDetector(this)
      this.resizeScrollDetector = new ResizeScrollDetector(this)
      if (this.options['background.enabled']) this.backgroundDetector.startDetection()

      if (this.options['ad.blockDetection']) {
        this.blockDetector = new BlockDetector(this)
      }
    },

    _checkOldData: function () {
      if (this._adapter && this._adapter.flags.isStarted) return
      if (this.infinity.infinityStarted && !this.infinity.infinityStopped) return
      this.restartViewTransform()
    },

    restartViewTransform: function (response) {
      // FastData
      this.viewTransform = new ViewTransform(this)
      this.viewTransform.on(ViewTransform.Event.DONE, this._receiveData.bind(this))

      // External response
      if (response) {
        this.viewTransform.setData(response)
        return
      }

      if (this.getIsDataExpired() ||
        ((this.storage.getLocal('accCode') !== this.options.accountCode) &&
          (this.storage.getSession('accCode') !== this.options.accountCode))) { // If expired or nonexistant
        this.storage.removeStorages('data')
        this.storage.removeStorages('session')
        this.storage.removeLocal('infinityStarted')
        this.storage.removeLocal('infinityStopped')
        this.viewTransform.init() // request a new data
      } else {
        this.viewTransform.setData(this.getStoredData()) // use stored data
      }
    },

    /**
    * This callback is called when a correct data response is received.
    *
    * @param {any} e Response from fastdata
    */
    _receiveData: function (e) {
      this._ping.interval = e.target.response.pingTime * 1000
      this._beat.interval = e.target.response.beatTime * 1000
      this.sessionExpire = e.target.response.sessionExpire * 1000
      this.storage.setStorages('data', e.target.response.msg)
      this.storage.setStorages('dataTime', new Date().getTime())
      this.storage.setStorages('accCode', this.options.accountCode)
      if (this.getIsSessionExpired()) {
        this.viewTransform.setSessionId(this.viewTransform.response.code)
        this.storage.setStorages('session', this.viewTransform.response.code)
      } else {
        this.viewTransform.setSessionId(this.getSessionId())
      }
    },

    /**
    * Reset all variables and stop all timers
    * @private
    */
    _reset: function () {
      this._stopPings()
      this.resourceTransform = new ResourceTransform(this)
      if (this._adapter) { // The fix
        this._adapter.flags.reset()
      }
      this.isInitiated = false
      this.isStarted = false
      this.startDelayed = false
      this.isAdsManifestSent = false
      this.initChrono.reset()
      this._totalPrerollsTime = 0
      this.requestBuilder.lastSent.breakNumber = 0
      this.requestBuilder.lastSent.adNumber = 0
      this._savedAdManifest = null
      this._savedAdError = null
      this.playedPostrolls = 0
      this.isBreakStarted = false
    },

    /**
    * Creates and enqueues related request using {@link Communication#sendRequest}.
    * It will fire will-send-events.
    *
    * @param {string} willSendEvent Name of the will-send event. Use {@link Plugin.Event} enum.
    * @param {string} service Name of the service. Use {@link Constants.Service} enum.
    * @param {Object} params Params of the request
    * @param {Object} body Body of the request, if it is a POST request
    * @param {string} method Request method. GET by default
    * @param {function} callback Callback method for successful request
    * @param {Object} callbackParams Json with params for callback call
    * @private
    */
    _send: function (willSendEvent, service, params, body, method, callback, callbackParams) {
      var now = new Date().getTime()
      if (this.options.preventZombieViews && this.lastEventTime && (now > (this.lastEventTime + (600 * 1000)))) { // 600 * 1000ms = 10 minutes
        // if last event was sent more than 10 minutes ago, it will use new view code
        this.viewTransform.nextView()
      }
      this.lastEventTime = (service === Constants.Service.STOP) ? null : now
      params = this.requestBuilder.buildParams(params, service)

      if (this.getIsLive()) {
        params.mediaDuration = undefined
        params.playhead = undefined
      }

      var data = {
        params: params,
        plugin: this,
        adapter: this.getAdapter(),
        adsAdapter: this.getAdsAdapter()
      }

      this.emit(willSendEvent, data)

      if (this._comm && (params !== null || typeof method !== 'undefined') && this.options.enabled) {
        this.lastServeiceSent = service
        var options = {}
        if (typeof method !== 'undefined' && method !== 'GET') {
          options.method = method
        }
        var request = new YBRequest(null, service, params, options)
        if (body) request.setBody(body)
        this._comm.sendRequest(request, callback, callbackParams)
      }
    },

    /**
    * Initializes comm and its transforms.
    * @private
    */
    _initComm: function () {
      this.resourceTransform.init(this.getResource())

      this._comm = new Communication()
      this._comm.addTransform(new FlowTransform())
      this._comm.addTransform(this.viewTransform)
      if (this.options && this.options.offline) {
        this._comm.addTransform(new OfflineTransform(this))
      } else {
        this._comm.addTransform(this.resourceTransform)
      }
    },

    /**
    * Returns the current {@link youbora.Communication} instance.
    *
    * @returns {youbora.Communication} communication instance
    */
    getComm: function () {
      return this._comm
    },

    /**
    * Modifies current options. See {@link Options.setOptions}.
    *
    * @param {any} options
    */
    setOptions: function (options) {
      if (options) {
        this.options.setOptions(options)
        if (typeof options['background.enabled'] === 'boolean') {
          if (options['background.enabled']) {
            this.backgroundDetector.startDetection()
          } else {
            this.backgroundDetector.stopDetection()
          }
        }
      }
    },

    /**
    * Disable request sending.
    */
    disable: function () {
      this.setOptions({ enabled: false })
    },

    /**
    * Re-enable request sending.
    */
    enable: function () {
      this.setOptions({ enabled: true })
    }
  },

  /** @lends youbora.Plugin */
  {
    // Static Memebers //
    /**
    * List of events that could be fired
    * @enum
    * @event
    */
    Event: Constants.WillSendEvent
  }
)

// Apply Mixins
// Plugin is actually a big class, I decided to separate the logic into
// different mixin files to ease the maintainability of each file.
// Filename convention will be plugin+xxxxx.js where xxxxx is the added functionality.
Util.assign(Plugin.prototype, require('./plugin+content'))
Util.assign(Plugin.prototype, require('./plugin+getters'))
Util.assign(Plugin.prototype, require('./plugin+content+getters'))
Util.assign(Plugin.prototype, require('./plugin+ads'))
Util.assign(Plugin.prototype, require('./plugin+ads+getters'))
Util.assign(Plugin.prototype, require('./plugin+pings'))
Util.assign(Plugin.prototype, require('./plugin+fire'))
Util.assign(Plugin.prototype, require('./plugin+infinity'))
Util.assign(Plugin.prototype, require('./plugin+infinity+getters'))

module.exports = Plugin
