var Log = require('../log')
var Constants = require('../constants')

var Adapter = require('../adapter/adapter')

// This file is designed to add extra functionalities to Plugin class

var PluginAdsMixin = {
  /**
     * Returns current adapter or null.
     *
     * @returns {Adapter}
     *
     * @memberof youbora.Plugin.prototype
     */
  getAdsAdapter: function () {
    return this._adsAdapter
  },

  /**
     * Sets an adapter for ads.
     *
     * @param {Adapter} adsAdapter
     *
     * @memberof youbora.Plugin.prototype
     */
  setAdsAdapter: function (adsAdapter) {
    if (adsAdapter.plugin) {
      Log.warn('Adapters can only be added to a single plugin')
    } else {
      this.removeAdsAdapter()
      adsAdapter.plugin = this
      this._adsAdapter = adsAdapter
      this._adsAdapter.on(Adapter.Event.START, this._adStartListener.bind(this))
      this._adsAdapter.on(Adapter.Event.JOIN, this._adJoinListener.bind(this))
      this._adsAdapter.on(Adapter.Event.PAUSE, this._adPauseListener.bind(this))
      this._adsAdapter.on(Adapter.Event.RESUME, this._adResumeListener.bind(this))
      this._adsAdapter.on(Adapter.Event.BUFFER_BEGIN, this._adBufferBeginListener.bind(this))
      this._adsAdapter.on(Adapter.Event.BUFFER_END, this._adBufferEndListener.bind(this))
      this._adsAdapter.on(Adapter.Event.STOP, this._adStopListener.bind(this))
      this._adsAdapter.on(Adapter.Event.ERROR, this._adErrorListener.bind(this))
      this._adsAdapter.on(Adapter.Event.CLICK, this._adClickListener.bind(this))
      this._adsAdapter.on(Adapter.Event.MANIFEST, this._adManifestListener.bind(this))
      this._adsAdapter.on(Adapter.Event.PODSTART, this._adBreakStartListener.bind(this))
      this._adsAdapter.on(Adapter.Event.PODSTOP, this._adBreakStopListener.bind(this))
      this._adsAdapter.on(Adapter.Event.QUARTILE, this._adQuartileListener.bind(this))
      this.resizeScrollDetector.startDetection()
    }
  },

  /**
     * Removes the current adapter. Fires stop if needed. Calls adapter.dispose()
     *
     * @memberof youbora.Plugin.prototype
     */
  removeAdsAdapter: function () {
    if (this._adsAdapter) {
      this._adsAdapter.dispose()
      this._adsAdapter.plugin = null
      this._adsAdapter.off(Adapter.Event.START, this._adStartListener)
      this._adsAdapter.off(Adapter.Event.JOIN, this._adJoinListener)
      this._adsAdapter.off(Adapter.Event.PAUSE, this._adPauseListener)
      this._adsAdapter.off(Adapter.Event.RESUME, this._adResumeListener)
      this._adsAdapter.off(Adapter.Event.BUFFER_BEGIN, this._adBufferBeginListener)
      this._adsAdapter.off(Adapter.Event.BUFFER_END, this._adBufferEndListener)
      this._adsAdapter.off(Adapter.Event.STOP, this._adStopListener)
      this._adsAdapter.off(Adapter.Event.ERROR, this._adErrorListener)
      this._adsAdapter.off(Adapter.Event.CLICK, this._adClickListener)
      this._adsAdapter.off(Adapter.Event.MANIFEST, this._adManifestListener)
      this._adsAdapter.off(Adapter.Event.PODSTART, this._adBreakStartListener)
      this._adsAdapter.off(Adapter.Event.PODSTOP, this._adBreakStopListener)
      this._adsAdapter.off(Adapter.Event.QUARTILE, this._adQuartileListener)
      this.resizeScrollDetector.stopDetection()
      this._adsAdapter = null
    }
  },

  // ---------------------------------------- LISTENERS -----------------------------------------
  _adStartListener: function (e) {
    if (this._adapter) {
      this._adapter.fireBufferEnd()
      this._adapter.fireSeekEnd()
      if (!this.isInitiated && !this._adapter.flags.isStarted) this._adapter.fireStart()
      if (this._adapter.flags.isPaused) this._adapter.chronos.pause.reset()
    } else {
      this.fireInit()
    }
    if (this._adsAdapter) {
      this._adsAdapter.fireManifest()
      this._adsAdapter.fireBreakStart()
      this._adsAdapter.chronos.viewedMax = []
    }
    var params = e.data.params || {}
    params.adNumber = this.requestBuilder.getNewAdNumber()
    params.breakNumber = this.requestBuilder.lastSent.breakNumber
    if (!this.options['ad.ignore']) {
      var allParamsReady = (this.getAdResource() || this.getAdTitle()) && typeof this.getAdDuration() === 'number'
      if (allParamsReady) {
        this.adStartSent = true
        this._send(Constants.WillSendEvent.WILL_SEND_AD_START, Constants.Service.AD_START, params)
        Log.notice(
          Constants.Service.AD_INIT + ' ' +
          params.adPosition + params.adNumber +
          ' at ' + params.playhead + 's'
        )
      } else {
        this.adInitSent = true
        this._send(Constants.WillSendEvent.WILL_SEND_AD_INIT, Constants.Service.AD_INIT, params)
        Log.notice(
          Constants.Service.AD_INIT + ' ' +
          params.adPosition + params.adNumber +
          ' at ' + params.playhead + 's'
        )
      }
    }
  },

  _adJoinListener: function (e) {
    var params = e.data.params || {}
    params.adNumber = this.requestBuilder.lastSent.adNumber
    params.breakNumber = this.requestBuilder.lastSent.breakNumber
    params.adPosition = this.requestBuilder.lastSent.adPosition
    if (!this.options['ad.ignore'] && this.adInitSent && !this.adStartSent) {
      this._send(Constants.WillSendEvent.WILL_SEND_AD_START, Constants.Service.AD_START, params)
      Log.notice(
        Constants.Service.AD_START + ' ' +
        params.adPosition + params.adNumber +
        ' at ' + params.playhead + 's'
      )
    }
    this._adsAdapter.startChronoView()
    if (this.adConnected) {
      this._adsAdapter.chronos.join.startTime = this.adConnectedTime
      this._adsAdapter.chronos.total.startTime = this.adConnectedTime
      this.adConnectedTime = 0
      this.adConnected = false
    }
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_JOIN, Constants.Service.AD_JOIN, params)
    Log.notice(Constants.Service.AD_JOIN + ' ' + params.adJoinDuration + 'ms')
    this.adInitSent = false
    this.adStartSent = false
  },

  _adPauseListener: function (e) {
    var params = e.data.params || {}
    params.adNumber = this.requestBuilder.lastSent.adNumber
    params.breakNumber = this.requestBuilder.lastSent.breakNumber
    this._adsAdapter.stopChronoView()
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_PAUSE, Constants.Service.AD_PAUSE, params)
    Log.notice(Constants.Service.AD_PAUSE + ' at ' + params.adPlayhead + 's')
  },

  _adResumeListener: function (e) {
    var params = e.data.params || {}
    params.adNumber = this.requestBuilder.lastSent.adNumber
    params.breakNumber = this.requestBuilder.lastSent.breakNumber
    params.adPosition = this.requestBuilder.lastSent.adPosition
    this._adsAdapter.startChronoView()
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_RESUME, Constants.Service.AD_RESUME, params)
    Log.notice(Constants.Service.AD_RESUME + ' ' + params.adPauseDuration + 'ms')
  },

  _adBufferBeginListener: function (e) {
    Log.notice('Ad Buffer Begin')
    this._adsAdapter.stopChronoView()
    if (this._adsAdapter && this._adsAdapter.flags.isPaused) {
      this._adsAdapter.chronos.pause.reset()
    }
  },

  _adBufferEndListener: function (e) {
    var params = e.data.params || {}
    this._adsAdapter.startChronoView()
    params.adNumber = this.requestBuilder.lastSent.adNumber
    params.breakNumber = this.requestBuilder.lastSent.breakNumber
    params.adPosition = this.requestBuilder.lastSent.adPosition
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_BUFFER, Constants.Service.AD_BUFFER, params)
    Log.notice(Constants.Service.AD_BUFFER + ' ' + params.adBufferDuration + 'ms')
  },

  _adStopListener: function (e) {
    this._adsAdapter.stopChronoView()
    this._adsAdapter.flags.reset()
    this._totalPrerollsTime = (this._totalPrerollsTime || 0) + this._adsAdapter.chronos.total.getDeltaTime()

    var params = e.data.params || {}
    params.adNumber = this.requestBuilder.lastSent.adNumber
    params.breakNumber = this.requestBuilder.lastSent.breakNumber
    if (this.requestBuilder.lastSent.adPosition === Constants.AdPosition.Postroll) {
      this.playedPostrolls++
    }
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_STOP, Constants.Service.AD_STOP, params)
    Log.notice(Constants.Service.AD_STOP + ' ' + params.adTotalDuration + 'ms')
    if (this.options['ad.expectedPattern'] && this.options['ad.expectedPattern'].post &&
      this.options['ad.expectedPattern'].post[0] === this.playedPostrolls && params.adPosition === Constants.AdPosition.Postroll) {
      this.fireStop()
    }

    this.adConnected = true
    this.adConnectedTime = new Date().getTime()
  },

  _adErrorListener: function (e) {
    var params = e.data.params || {}
    if (this._adapter && !this._adapter.flags.isStarted && !this.isInitiated) {
      this._savedAdError = e
      return null // Ignore ad errors before content
    }
    if (this._blockAdError(e.data.params)) return null
    if (this._adsAdapter) {
      this._adsAdapter.fireManifest()
      this._adsAdapter.fireBreakStart()
    }
    if (!this._adsAdapter || !this._adsAdapter.flags.isStarted) {
      params.adNumber = this.requestBuilder.getNewAdNumber()
    } else {
      params.adNumber = this.requestBuilder.lastSent.adNumber
    }
    params.breakNumber = this.requestBuilder.lastSent.breakNumber
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_ERROR, Constants.Service.AD_ERROR, params)
    Log.notice(Constants.Service.AD_ERROR)
  },

  _adSavedError: function () {
    if (this._savedAdError) {
      this._adErrorListener(this._savedAdError)
      this._savedAdError = null
    }
  },

  _adSavedManifest: function () {
    if (this._savedAdManifest) {
      this._adManifestListener(this._savedAdManifest)
      this._savedAdManifest = null
    }
  },

  _blockAdError: function (errorParams) {
    var now = Date.now()
    var sameError = this._lastAdErrorParams
      ? this._lastAdErrorParams.errorCode === errorParams.errorCode && this._lastAdErrorParams.msg === errorParams.msg : false
    if (sameError && this._lastAdErrorTime + 5000 > now) {
      this._lastAdErrorTime = now
      return true
    }
    this._lastAdErrorTime = now
    this._lastAdErrorParams = errorParams
    return false
  },

  _adClickListener: function (e) {
    var params = e.data.params || {}
    params.adNumber = this.requestBuilder.lastSent.adNumber
    params.breakNumber = this.requestBuilder.lastSent.breakNumber
    params.adPosition = this.requestBuilder.lastSent.adPosition
    this._adsAdapter.stopChronoView()
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_CLICK, Constants.Service.AD_CLICK, params)
    Log.notice(Constants.Service.AD_CLICK)
  },

  _adManifestListener: function (e) {
    if (!this.isAdsManifestSent) {
      if (this._adapter && !this._adapter.flags.isStarted && !this.isInitiated) {
        this._savedAdManifest = e
        return null // Ignore ad manifest before content
      }
      var params = e.data.params || {}
      this.isAdsManifestSent = true
      if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_MANIFEST, Constants.Service.AD_MANIFEST, params)
      Log.notice(Constants.Service.AD_MANIFEST)
    }
  },

  _adBreakStartListener: function (e) {
    var params = e.data.params || {}
    this.requestBuilder.lastSent.breakNumber ? this.requestBuilder.lastSent.breakNumber++ : this.requestBuilder.lastSent.breakNumber = 1
    params.breakNumber = this.requestBuilder.lastSent.breakNumber
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_POD_START, Constants.Service.AD_POD_START, params)
    Log.notice(Constants.Service.AD_POD_START)
    this.adConnected = false
  },

  _adBreakStopListener: function (e) {
    var params = e.data.params || {}
    params.breakNumber = this.requestBuilder.lastSent.breakNumber
    params.breakPosition = this.requestBuilder.lastSent.breakPosition
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_POD_STOP, Constants.Service.AD_POD_STOP, params)
    Log.notice(Constants.Service.AD_POD_STOP)
    this.adConnected = false
  },

  _adQuartileListener: function (e) {
    var params = e.data.params || {}
    if (!params.quartile) return
    params.adNumber = this.requestBuilder.lastSent.adNumber
    params.adPosition = this.requestBuilder.lastSent.adPosition
    params.breakNumber = this.requestBuilder.lastSent.breakNumber
    params.breakPosition = this.requestBuilder.lastSent.breakPosition
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_QUARTILE, Constants.Service.AD_QUARTILE, params)
    Log.notice(Constants.Service.AD_QUARTILE)
  }

}

module.exports = PluginAdsMixin
