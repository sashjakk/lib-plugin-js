var YouboraObject = require('../object')
var DeprecatedOptions = require('./deprecatedOptions')
var Log = require('../log')

var Options = YouboraObject.extend(
  /** @lends youbora.Options.prototype */
  {
    /**
     * This Class store youbora configuration settings.
     * Any value specified in this class, if set, will override the info the plugin is able to get
     * on its own.
     *
     * @constructs Options
     * @param {Object|Options} [options] A literal containing values.
     * @extends youbora.YouboraObject
     * @memberof youbora
     */
    constructor: function (options) {
      /** @prop {boolean} [enabled=true] If false, the plugin won't send NQS requests. */
      this.enabled = true

      /** @prop {string} [host='a-fds.youborafds01.com'] Host of the Fastdata service. */
      this.host = 'a-fds.youborafds01.com'

      /**
      * @prop {string} [accountCode='nicetest']
      * NicePeopleAtWork account code that indicates the customer account.
      */
      this.accountCode = 'nicetest'

      /**
      *  @prop {boolean} [preventZombieViews=true]
      * If true, the plugin will check if the last event
      * was sent more than 10 mins ago
      * so it will not send more events to the same view
      */
      this.preventZombieViews = true

      /**
      * @prop {boolean} [offline=false]
      * If true the plugin will store the events and send them later when there's connection
      */
      this.offline = false

      /** @prop {string} [referer] Site url.
      *  By default window.location.href */
      this.referer = null

      /**
      * @prop {boolean} [disableCookies]
      * To disable cookies storage fallback after local/sessionstorage
      * True by default
      */
      this.disableCookies = true

      // USER

      /**
      * @prop {string} [user.email]
      * User email.
      */
      this['user.email'] = null

      /**
      * @prop {string} [user.type]
      * User type.
      */
      this['user.type'] = null

      /**
      * @prop {string} [user.name]
      * User ID value inside your system.
      */
      this['user.name'] = null

      /**
      *  @prop {boolean} [user.obfuscateIp=false]
      * If true, the view will have the IP obfuscated
      */
      this['user.obfuscateIp'] = false

      /**
      * @prop {string} [user.anonymousId]
      * Anonymous identifyer of the user provided by the customer.
      */
      this['user.anonymousId'] = null

      // PARSERS

      /**
      * @prop {boolean} [parse.Hls=false]
      * If true the plugin will parse HLS files to use the first .ts file found as resource.
      * It might slow performance down.
      */
      this['parse.hls'] = false

      /**
      * @prop {string} [parse.CdnNameHeader]
      * If defined, resource parse will try to fetch the CDN code from the custom header defined
      * by this property. ie: 'x-cdn-forward'
      */
      this['parse.cdnNameHeader'] = 'x-cdn-forward'

      /**
      * @prop {boolean} [parse.CdnNode=false]
      * If true the plugin will query the CDN to retrieve the node name.
      * It might slow performance down.
      */
      this['parse.cdnNode'] = false

      /**
      * @prop {array<string>} [parse.CdnNode.list=false]
      * If true the plugin will query the CDN to retrieve the node name.
      * It might slow performance down.
      */
      this['parse.cdnNode.list'] = ['Akamai', 'Cloudfront', 'Level3', 'Fastly', 'Highwinds', 'Telefonica']

      /**
      * @prop {boolean} [parse.CdnNode=false]
      * If true the plugin will look for location value in manifest header to retrieve the actual resource
      * It might slow performance down.
      */
      this['parse.locationHeader'] = false

      /**
      * @prop {boolean} [parse.dash=false]
      * If true the plugin will look for location and segment values inside dash manifest to retrieve the actual resource
      * It might slow performance down.
      */
      this['parse.dash'] = false

      /**
       * @prop {function} [parse.fdsResponseHost=null]
       * Parses fastdata response to modify the host where the requests will be sent
       */
      this['parse.fdsResponseHost'] = null

      // NETWORK

      /** @prop {string} [network.ip] IP of the viewer/user. ie= '100.100.100.100'. */
      this['network.ip'] = null

      /** @prop {string} [network.isp] Name of the internet service provider of the viewer/user. */
      this['network.isp'] = null

      /**
      * @prop {string} [network.connectionType]
      * Type of connection used
      */
      this['network.connectionType'] = null

      // DEVICE

      /**
      * @prop {string} [device.code]
      * Youbora's device code. If specified it will rewrite info gotten from user agent.
      * See a list of codes in {@link http://mapi.youbora.com:8081/devices}
      */
      this['device.code'] = null

      /**
      * @prop {string} [device.model]
      * Device model name
      */
      this['device.model'] = null

      /**
      * @prop {string} [device.brand]
      * Device vendor name
      */
      this['device.brand'] = null

      /**
      * @prop {string} [device.type]
      * Device type (pc, smartphone, stb, tv, etc.)
      */
      this['device.type'] = null

      /**
      * @prop {string} [device.name]
      * Device name. It must exist in NPAW database.
      */
      this['device.name'] = null

      /**
      * @prop {string} [device.osName]
      * OS name.
      */
      this['device.osName'] = null

      /**
      * @prop {string} [device.osVersion]
      * OS version.
      */
      this['device.osVersion'] = null

      /**
      * @prop {string} [device.browserName]
      * Browser name.
      */
      this['device.browserName'] = null

      /**
      * @prop {string} [device.browserVersion]
      * Browser version.
      */
      this['device.browserVersion'] = null

      /**
      * @prop {string} [device.browserType]
      * Browser type.
      */
      this['device.browserType'] = null

      /**
      * @prop {string} [device.browserEngine]
      * Browser engine.
      */
      this['device.browserEngine'] = null

      /**
      * @prop {bool} [device.isAnonymous]
      * If true, it blocks 'deviceUUID' parameter in requests.
      */
      this['device.isAnonymous'] = false

      // CONTENT

      /** @prop {string} [content.transactionCode] Custom unique code to identify the view. */
      this['content.transactionCode'] = null

      /** @prop {string} [content.resource] URL/path of the current media resource. */
      this['content.resource'] = null

      /** @prop {boolean} [content.isLive] True if the content is live false if VOD. */
      this['content.isLive'] = null

      /** @prop {boolean} [content.isLive.noSeek] True if the player seeks automatically when resumed or ending buffer. Only for live content */
      this['content.isLive.noSeek'] = false

      /** @prop {string} [content.title] Title of the media. */
      this['content.title'] = null

      /** @prop {string} [content.program] Secondary title of the media */
      this['content.program'] = null

      /** @prop {number} [content.duration] Duration of the media. */
      this['content.duration'] = null

      /** @prop {int} [content.fps] Frames per second of the content in the current moment. */
      this['content.fps'] = null

      /** @prop {int} [content.bitrate] Bitrate of the content in bits per second. */
      this['content.bitrate'] = null

      /** @prop {int} [content.throughput] Throughput of the client bandwith in bits per second. */
      this['content.throughput'] = null

      /** @prop {string} [content.rendition] Name of the current rendition of the content. */
      this['content.rendition'] = null

      /**
       * @prop {string} [content.cdn]
       * Codename of the CDN where the content is streaming from.
       * See a list of codes in {@link http://mapi.youbora.com:8081/cdns}
       * */
      this['content.cdn'] = null

      /** @prop {string} [content.cdnNode] CDN node id */
      this['content.cdnNode'] = null

      /** @prop {int} [content.cdnType] CDN node content access type
       * It defines if the content request hits the cache or not
       * TCP_HIT / HIT: 1
       * TCP_MISS / MISS: 2
       * TCP_MEM_HIT: 3
       * TCP_IMS_HIT: 4
       */
      this['content.cdnType'] = null

      /**
       * @prop {object} [content.metadata]
       * Item containing mixed extra information about the content like: director, parental rating,
       * device info or the audio channels.This object may store any serializable key:value info.
       */
      this['content.metadata'] = {}

      /**
       * @prop {object} [content.metrics]
       * Item containing metrics in json format. Reported every ping if the values change
       */
      this['content.metrics'] = {}

      /** @prop {string} [content.streamingProtocol] Name of the streaming media protocol.
       * Can be:
       *   - HDS (Adobe HDS)
       *   - HLS (Apple HLS)
       *   - MSS (Microsoft Smooth Streaming)
       *   - DASH (MPEG-DASH)
       *   - RTMP (Adobe RTMP)
       *   - RTP (RTP)
       *   - RTSP (RTSP)
       */
      this['content.streamingProtocol'] = null

      /** @prop {number} [content.package] Package of the media. */
      this['content.package'] = null

      /** @prop {number} [content.saga] Saga of the media. */
      this['content.saga'] = null

      /** @prop {number} [content.tvShow] TV Show of the media. */
      this['content.tvShow'] = null

      /** @prop {number} [content.season] Season of the media. */
      this['content.season'] = null

      /** @prop {number} [content.episodeTitle] Episode title of the media. */
      this['content.episodeTitle'] = null

      /** @prop {number} [content.channel] Channel name of the media. */
      this['content.channel'] = null

      /** @prop {number} [content.id] ID of the media. */
      this['content.id'] = null

      /** @prop {number} [content.imdbId] IMDB id of the media. */
      this['content.imdbId'] = null

      /** @prop {number} [content.gracenoteId] Gracenote id of the media. */
      this['content.gracenoteId'] = null

      /** @prop {number} [content.type] Type of the media. */
      this['content.type'] = null

      /** @prop {number} [content.genre] Genre of the media. */
      this['content.genre'] = null

      /** @prop {number} [content.language] Language of the media. */
      this['content.language'] = null

      /** @prop {number} [content.subtitles] Subtitles of the media. */
      this['content.subtitles'] = null

      /** @prop {number} [content.contractedResolution] Contracted Resolution of the media. */
      this['content.contractedResolution'] = null

      /** @prop {number} [content.cost] Cost of the media. */
      this['content.cost'] = null

      /** @prop {number} [content.price] Price of the media. */
      this['content.price'] = null

      /** @prop {number} [content.playbackType] Type of the media. Can be Vod, Live, catch-up or offline */
      this['content.playbackType'] = null

      /** @prop {number} [content.drm] DRM of the media. */
      this['content.drm'] = null

      // Encoding

      /** @prop {number} [content.encoding.videoCodec] Video codec of the media. */
      this['content.encoding.videoCodec'] = null

      /** @prop {number} [content.encoding.audioCodec] Audio codec of the media. */
      this['content.encoding.audioCodec'] = null

      /** @prop {number} [content.encoding.codecSettings] Codec settings of the media. */
      this['content.encoding.codecSettings'] = null

      /** @prop {number} [content.encoding.codecProfile] Codec profile of the media. */
      this['content.encoding.codecProfile'] = null

      /** @prop {number} [content.encoding.containerFormat] Container format of the media. */
      this['content.encoding.containerFormat'] = null

      // ADS

      /**
      * @prop {object} [ad.metadata]
      * Item containing mixed extra information about ads like: request url.
      * This object may store any serializable key:value info.
      */
      this['ad.metadata'] = {}

      /**
      * @prop {string} [ad.campaign]
      * String containing the name of the campaign
      */
      this['ad.campaign'] = null

      /**
      * @prop {string} [ad.creativeId]
      * String containing the id of the creative
      */
      this['ad.creativeId'] = null

      /**
      * @prop {string} [ad.provider]
      * String containing the provider of the ad
      */
      this['ad.provider'] = null

      /**
      * @prop {string} [ad.resource]
      * String containing the ad resource
      */
      this['ad.resource'] = null

      /**
      * @prop {string} [ad.title]
      * String containing the title of the campaign
      */
      this['ad.title'] = null

      /**
      * @prop {object} [ad.expectedPattern]
      * Json with the position of the breaks expected.
      * Arrays are the number of breaks, and the numbers in them, the number of ads for each break
      *
      * Example:
      * {pre: [1],
      * mid: [1,2],
      * post: [1]}
      * Would be a view with 1 preroll break with 1 ad, 2 midroll breaks, one with 1 ad and
      * the other with 2, and one postroll break with 1 ad.
      */
      this['ad.expectedPattern'] = null

      /**
      * @prop {string} [ad.givenAds]
      * Number of ads given by the adserver for this break
      */
      this['ad.givenAds'] = null

      /**
      * @prop {array<int>} [ad.breaksTime]
      * Array of ints for the time position of adbreaks
      */
      this['ad.breaksTime'] = null

      /**
      * @prop {string} [ad.expectedBreaks]
      * Number of breaks expected for the view
      */
      this['ad.expectedBreaks'] = null

      /**
      * @prop {int} [ad.givenBreaks]
      * Number of breaks given by the adserver for the view
      */
      this['ad.givenBreaks'] = null

      /**
      * @prop {boolean} [ad.ignore]
      * False by default.
      * If true, youbora blocks ad events and calculates jointime ignoring ad time.
      */
      this['ad.ignore'] = false

      /**
      * @prop {boolean} [ad.blockDetection]
      * False by default.
      * If true, youbora sends a request to an url that looks like an ad to check if it has response.
      */
      this['ad.blockDetection'] = false

      // APP

      /**
      * @prop {string} [app.name]
      * String containing the name of the app
      */
      this['app.name'] = null

      /**
      * @prop {string} [app.releaseVerson]
      * String containing the app version
      */
      this['app.releaseVersion'] = null

      /**
      * @prop {boolean} [app.https=null]
      * Define the security of NQS calls.
      * If true it will use 'https://',
      * if false it will use 'http://',
      * if null/undefined it will use '//'.
      */
      this['app.https'] = false

      // BACKGROUND

      /**
      *  @prop {boolean} [background.enabled=false]
      * If true, plugin will send background/foreground events
      * Different device behaviour is settable in background.settings
      */
      this['background.enabled'] = true

      /**
      *  @prop {string} [background.settings]
      * Action to do when the browser goes to background.
      * Options are 'stop', 'pause', and '' for no action.
      * stop will be used to stop the view and track post-foreground events in a new view
      * pause will be used when after foreground event, an action like pressing play button is expected to resume the content
      * '' will be used if the content can be played in background
      * If not defined, specific device options will be used
      * background.setings.android / background.settings.iOS / background.settings.desktop / background.settings.tv
      * Default specific device values are stop for android and iphone, nothing for desktop.
      */
      this['background.settings'] = null

      /**
      *  @prop {string} [background.settings.android='stop']
      * If background.settings is not defined, action to do when the browser goes to background if
      * the device is android type.
      * Options are 'stop', 'pause', and '' or not defined for no action.
      * bg by default
      */
      this['background.settings.android'] = 'stop'

      /**
      *  @prop {string} [background.settings.iOS='stop']
      * If background.settings is not defined, action to do when the browser goes to background if
      * the device is iphone type.
      * Options are 'stop', 'pause', and '' or not defined for no action.
      * bg by default
      */
      this['background.settings.iOS'] = 'stop'

      /**
      *  @prop {string} [background.settings.desktop=null]
      * If background.settings is not defined, action to do when the browser goes to background if
      * the device is desktop type.
      * Options are 'stop', 'pause', and '' or not defined for no action.
      * Null by default
      */
      this['background.settings.desktop'] = null

      /**
      *  @prop {string} [background.settings.tv='stop']
      * If background.settings is not defined, action to do when the browser goes to background if
      * the device is smartTV type.
      * Options are 'stop', 'pause', and '' or not defined for no action.
      * bg by default
      */
      this['background.settings.tv'] = 'stop'

      /**
      *  @prop {string} [background.settings.playstation='stop']
      * If background.settings is not defined, action to do when the browser goes to background if
      * the device is playstation type.
      * Options are 'stop', 'pause', and '' or not defined for no action.
      * Null by default
      */
      this['background.settings.playstation'] = 'stop'

      // SMARTSWITCH

      /**
      *  @prop {string} [smartswitch.configCode]
      * Config code for smartswitch
      * null by default
      */
      this['smartswitch.configCode'] = null

      /**
      *  @prop {string} [smartswitch.groupCode]
      * Group code for smartswitch
      * null by default
      */
      this['smartswitch.groupCode'] = null

      /**
      *  @prop {string} [smartswitch.contractCode]
      * Contract code for smartswitch
      * null by default
      */
      this['smartswitch.contractCode'] = null

      // EXTRAPARAMS // CUSTOM DIMENSIONS

      /** @prop {string} [content.customDimension.1] Custom parameter 1. */
      this['content.customDimension.1'] = null

      /** @prop {string} [content.customDimension.2] Custom parameter 2. */
      this['content.customDimension.2'] = null

      /** @prop {string} [content.customDimension.3] Custom parameter 3. */
      this['content.customDimension.3'] = null

      /** @prop {string} [content.customDimension.4] Custom parameter 4. */
      this['content.customDimension.4'] = null

      /** @prop {string} [content.customDimension.5] Custom parameter 5. */
      this['content.customDimension.5'] = null

      /** @prop {string} [content.customDimension.6] Custom parameter 6. */
      this['content.customDimension.6'] = null

      /** @prop {string} [content.customDimension.7] Custom parameter 7. */
      this['content.customDimension.7'] = null

      /** @prop {string} [content.customDimension.8] Custom parameter 8. */
      this['content.customDimension.8'] = null

      /** @prop {string} [content.customDimension.9] Custom parameter 9. */
      this['content.customDimension.9'] = null

      /** @prop {string} [content.customDimension.10] Custom parameter 10. */
      this['content.customDimension.10'] = null

      /** @prop {string} [content.customDimension.11] Custom parameter 11. */
      this['content.customDimension.11'] = null

      /** @prop {string} [content.customDimension.12] Custom parameter 12. */
      this['content.customDimension.12'] = null

      /** @prop {string} [content.customDimension.13] Custom parameter 13. */
      this['content.customDimension.13'] = null

      /** @prop {string} [content.customDimension.14] Custom parameter 14. */
      this['content.customDimension.14'] = null

      /** @prop {string} [content.customDimension.15] Custom parameter 15. */
      this['content.customDimension.15'] = null

      /** @prop {string} [content.customDimension.16] Custom parameter 16. */
      this['content.customDimension.16'] = null

      /** @prop {string} [content.customDimension.17] Custom parameter 17. */
      this['content.customDimension.17'] = null

      /** @prop {string} [content.customDimension.18] Custom parameter 18. */
      this['content.customDimension.18'] = null

      /** @prop {string} [content.customDimension.19] Custom parameter 19. */
      this['content.customDimension.19'] = null

      /** @prop {string} [content.customDimension.20] Custom parameter 20. */
      this['content.customDimension.20'] = null

      /** @prop {string} [ad.customDimension.1] Ad custom parameter 1. */
      this['ad.customDimension.1'] = null

      /** @prop {string} [ad.customDimension.2] Ad custom parameter 2. */
      this['ad.customDimension.2'] = null

      /** @prop {string} [ad.customDimension.3] Ad custom parameter 3. */
      this['ad.customDimension.3'] = null

      /** @prop {string} [ad.customDimension.4] Ad custom parameter 4. */
      this['ad.customDimension.4'] = null

      /** @prop {string} [ad.customDimension.5] Ad custom parameter 5. */
      this['ad.customDimension.5'] = null

      /** @prop {string} [ad.customDimension.6] Ad custom parameter 6. */
      this['ad.customDimension.6'] = null

      /** @prop {string} [ad.customDimension.7] Ad custom parameter 7. */
      this['ad.customDimension.7'] = null

      /** @prop {string} [ad.customDimension.8] Ad custom parameter 8. */
      this['ad.customDimension.8'] = null

      /** @prop {string} [ad.customDimension.9] Ad custom parameter 9. */
      this['ad.customDimension.9'] = null

      /** @prop {string} [ad.customDimension.10] Ad custom parameter 10. */
      this['ad.customDimension.10'] = null

      /** @prop {bool} [forceInit] Forces init to be sent instead of start, use it when mediaduration,
      *  title, source or is live is reported with a wrong value by the player until jointime happens */
      this.forceInit = false

      /**
      * @prop {object} [session.metrics]
      * Item containing metrics in json format. Reported every beat if the values change
      */
      this['session.metrics'] = {}

      /**
      * @prop {bool} [session.context]
      * Boolean to choose to report context or not. False by default
      */
      this['session.context'] = false

      /**
      * @prop {bool} [waitForMetadata]
      * Boolean to delay the start event. Use with `pendingMetadata`
      */
      this.waitForMetadata = false

      /**
      * @prop {array<string>} [pendingMetadata]
      * List of values that should be ready to send in start event. Use with `waitForMetadata` set to True.
      */
      this.pendingMetadata = []

      this.setOptions(options)
    },

    /**
    * Recursively sets the properties present in the params object.
    * ie: this.username = params.username.
    *
    * @param {Object} options A literal or another Data containing values.
    * @param {Object} [base=this] Start point for recursion.
    */
    setOptions: function (options, base) {
      var isInBase = false
      if (base === undefined) {
        base = this
        isInBase = true
        var deprecatedOptions = new DeprecatedOptions()
      }
      if (typeof options !== 'undefined') {
        for (var key in options) {
          var deprecated = false
          var newKey = null
          if (isInBase) {
            if (!this.hasOwnProperty(key)) { // eslint-disable-line no-prototype-builtins
              if (deprecatedOptions.exists(key)) {
                newKey = deprecatedOptions.getNewName(key)
                deprecated = true
              } else {
                Log.warn('The option "' + key + '" does not exist, so it cannot be set')
              }
            }
          }
          if ((typeof base[key] === 'object' && base[key] !== null) && (!Array.isArray(base[key]) && key === 'parse.cdnNode.list')) {
            this.setOptions(options[key], base[key])
          } else {
            if (deprecated) {
              base[newKey] = options[key]
            } else {
              base[key] = options[key]
            }
          }
        }
      }
    },

    /**
    * Sets all the values given in an array as extraparams. Limit is 20
    * @param {array<string>} paramsArray array of extraparam strings
    * */
    setExtraParams: function (paramsArray) {
      var maxLength = 20
      if (typeof paramsArray !== 'object' || !paramsArray.length) return
      if (paramsArray.length >= maxLength) paramsArray = paramsArray.slice(0, maxLength)
      while (paramsArray.length < maxLength) {
        paramsArray.push(null)
      }
      paramsArray.forEach(function (param, index) {
        this['content.customDimension.' + (index + 1).toString()] = param
      }.bind(this))
    },

    /**
    * @alias youbora.options.prototype.setExtraParams.
    */
    setCustomDimensions: function () {
      Options.prototype.setExtraParams.apply(this, arguments)
    },

    /**
    * Sets all the values given in an array as extraparams. Limit is 10
    * @param {array<string>} paramsArray array of extraparam strings
    * */
    setAdExtraParams: function (paramsArray) {
      var maxLength = 10
      if (typeof paramsArray !== 'object' || !paramsArray.length) return
      if (paramsArray.length >= maxLength) paramsArray = paramsArray.slice(0, maxLength)
      while (paramsArray.length < maxLength) {
        paramsArray.push(null)
      }
      paramsArray.forEach(function (param, index) {
        this['ad.customDimension.' + (index + 1).toString()] = param
      }.bind(this))
    },

    /**
    * @alias youbora.options.prototype.setAdExtraParams.
    */
    setAdCustomDimensions: function () {
      Options.prototype.setAdExtraParams.apply(this, arguments)
    }
  }
)

module.exports = Options
