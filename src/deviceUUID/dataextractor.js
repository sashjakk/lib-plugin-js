/* global ActiveXObject */
var YouboraObject = require('../object')

var DataExtractor = YouboraObject.extend({
  constructor: function (plugin) {
    this.plugin = plugin
  },

  getAllData: function () {
    var returnValue = this.getNonRandomData()
    returnValue.timestamp = this.getTimestamp()
    return returnValue
  },

  getNonRandomData: function () {
    var returnValue = {}
    returnValue.userAgent = this.getUserAgent()
    returnValue.threads = this.getVirtualCores()
    returnValue.language = this.getLanguage()
    returnValue.langList = this.getAvailableLanguages()
    returnValue.resolution = this.getResolution()
    returnValue.colorDepth = this.getColorDepth()
    returnValue.deviceMemory = this.getMemory()
    returnValue.touchscreen = this.getTouchscreen()
    returnValue.localStorage = this.getLocalStorage()
    returnValue.sessionStorage = this.getSessionStorage()
    returnValue.cookiesAvailable = this.getCookiesAvailable()
    returnValue.flashAvailable = this.getHasFlash()
    returnValue.timeZone = this.getTimeZone()
    returnValue.plugins = this.getPluginList()
    return returnValue
  },

  // Getters
  getUserAgent: function () {
    try {
      return this._navigatorCheck() ? navigator.userAgent : null
    } catch (err) {
      return null
    }
  },

  getVirtualCores: function () {
    try {
      return this._navigatorCheck() ? navigator.hardwareConcurrency : null
    } catch (err) {
      return null
    }
  },

  getLanguage: function () {
    try {
      return this._navigatorCheck() ? navigator.language : null
    } catch (err) {
      return null
    }
  },

  getAvailableLanguages: function () {
    try {
      return this._navigatorCheck() ? navigator.languages : null
    } catch (err) {
      return null
    }
  },

  getResolution: function () {
    try {
      if (this._navigatorCheck() && navigator.screen) {
        return navigator.screen.width.toString() + navigator.screen.height.toString()
      }
      return null
    } catch (err) {
      return null
    }
  },

  getColorDepth: function () {
    try {
      return (this._navigatorCheck() && navigator.screen) ? navigator.screen.colorDepth : null
    } catch (err) {
      return null
    }
  },

  getMemory: function () {
    try {
      return this._navigatorCheck() ? navigator.deviceMemory : null
    } catch (err) {
      return null
    }
  },

  getTouchscreen: function () {
    try {
      return this._navigatorCheck() ? navigator.maxTouchPoints : null
    } catch (err) {
      return false
    }
  },

  getLocalStorage: function () {
    try {
      return typeof localStorage !== 'undefined'
    } catch (err) {
      return false
    }
  },

  getSessionStorage: function () {
    try {
      return typeof sessionStorage !== 'undefined'
    } catch (err) {
      return false
    }
  },

  getCookiesAvailable: function () {
    try {
      return this._navigatorCheck() ? navigator.cookieEnabled : false
    } catch (err) {
      return false
    }
  },

  getHasFlash: function () {
    try {
      if (!this._navigatorCheck()) return false
      return (typeof navigator.plugins !== 'undefined' && typeof navigator.plugins['Shockwave Flash'] === 'object') || (typeof window !== 'undefined' && window.ActiveXObject && (new ActiveXObject('ShockwaveFlash.ShockwaveFlash')) !== false)
    } catch (err) {
      return false
    }
  },

  getPluginList: function () {
    try {
      if (!this._navigatorCheck() || !navigator.plugins || navigator.plugins.length === 0) return null
      var pluginlist = ''
      for (var counter = 0; counter < navigator.plugins.length; counter++) {
        pluginlist += navigator.plugins[counter].description + ' ' + navigator.plugins[counter].filename + ' ' + navigator.plugins[counter].name + ' '
      }
      return pluginlist
    } catch (err) {
      return null
    }
  },

  getTimeZone: function () {
    try {
      var date = new Date()
      return date.getTimezoneOffset().toString()
    } catch (err) {
      return null
    }
  },

  getTimestamp: function () {
    return new Date().getTime()
  },

  _navigatorCheck: function () {
    return typeof navigator !== 'undefined'
  }

})

module.exports = DataExtractor
