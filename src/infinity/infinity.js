/* global btoa */
var Emitter = require('../emitter')
var Comm = require('../comm/communication')
var WrongTransform = require('../comm/transform/wrong')

var YouboraInfinity = Emitter.extend(
  /** @lends youbora.Infinity.prototype */
  {

    /**
     * This class is the base of youbora infinity. Every plugin will have an instance.
     *
     * @param {youbora.Plugin} plugin Parent plugin.
     *
     * @constructs youbora.Infinity
     * @extends youbora.Emitter
     * @memberof youbora
     */
    constructor: function (plugin) {
      /** Parent {@link youbora.Plugin} reference. */
      this._plugin = plugin
    },

    /**
     * @alias youbora.Infinity.prototype.begin.
     */
    andBeyond: function () {
      YouboraInfinity.prototype.begin.apply(this, arguments)
    },

    /**
     * This method will start infinity logic, setting storage as needed.
     * Will call fireSessionStart the first time and fireNav for every subsequent route change.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    begin: function (dimensions) {
      this._comm = new Comm(this._plugin)
      this._comm.addTransform(this._plugin.viewTransform)
      this._comm.addTransform(new WrongTransform(this._plugin))
      if (this._plugin && this._plugin.storage && typeof this._plugin.storage.getLocal === 'function') {
        this._registeredProperties = this._plugin.storage.getLocal('inifnityRegisteredProperties')
      }
      if (!this._plugin.getIsSessionExpired() && this._plugin.storage.getLocal('infinityStarted')) {
        this.fireNav(dimensions) // returning
        this._sendExtraBeat()
      } else {
        this.fireSessionStart(dimensions) // first time
      }
    },

    _generateNewContext: function () {
      var context = null
      try {
        context = btoa(new Date().getTime())
      } catch (err) {
        context = 'Default'
      }
      this._plugin.storage.setSession('context', context)
    },

    _setLastActive: function () {
      if (!this._firstActive) {
        this._firstActive = this.getFirstActive()
      }
      this._plugin.storage.setStorages('lastactive', new Date().getTime())
    },

    getFirstActive: function () {
      return Number(this._plugin.getLastActive()) || 0
    },

    /**
     * Returns the current {@link youbora.Communication} instance.
     *
     * @returns {youbora.Communication} communication instance
     */
    getComm: function () {
      return this._comm
    },

    // Fire
    /**
     * Emits session start request.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireSessionStart: function (dimensions) {
      this._plugin.storage.setLocal('infinityStarted', 'true')
      this._plugin.storage.removeLocal('infinityStopped')
      this.infinityStarted = true
      this.infinityStopped = false
      this._generateNewContext()
      this.emit(YouboraInfinity.Event.SESSION_START, this._getParamsJson(dimensions, null, null, true, true))
      this._setLastActive()
    },

    /**
     * Emits session start request.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireSessionStop: function (params) {
      this.infinityStopped = true
      this.emit(YouboraInfinity.Event.SESSION_STOP, params)
      this._plugin.storage.removeStorages('data')
      this._plugin.storage.removeStorages('session')
      this._plugin.storage.removeStorages('lastactive')
    },

    /**
     * Stops the session and creates a new one
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    newSession: function (options, params) {
      this.fireSessionStop()
      this._plugin.storage.removeLocal('data')
      this._plugin.setOptions(options)
      this._plugin.restartViewTransform()
      this.fireSessionStart(params)
    },

    /**
     * Emits session start request.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireNav: function (dimensions) {
      if (!this.isActive()) return
      if (!this._plugin.getContext()) this._generateNewContext()
      this.emit(YouboraInfinity.Event.NAV, this._getParamsJson(dimensions, null, null, true))
    },

    _sendExtraBeat: function () {
      var now = new Date().getTime()
      if (this._plugin && this._plugin._beat) {
        var time = this._plugin._beat.chrono.startTime ? (now - this._plugin._beat.chrono.startTime) : 0
        this._plugin._sendBeat(time)
        this._plugin._beat.chrono.startTime = now
      }
      this._setLastActive()
    },

    /**
     * Emits session start request.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireEvent: function (eventName, dimensions, values) {
      this.emit(YouboraInfinity.Event.EVENT, this._getParamsJson(dimensions, values, eventName))
      this._setLastActive()
    },

    /** Register properties sent by the User, to send in all the events
    *
    * @memberof youbora.Infinity.prototype
    */
    register: function (dimensions, values) {
      this._registeredProperties = { dimensions: dimensions, values: values }
      this._plugin.storage.setLocal('inifnityRegisteredProperties', JSON.stringify(this._registeredProperties))
    },

    /** Calls register if registeredProperties is empty
    *
    * @memberof youbora.Infinity.prototype
    */
    registerOnce: function (dimensions, values) {
      if (!this._registeredProperties) {
        this.register(dimensions, values)
      }
    },

    /** Unregister properties registered with register()
    *
    * @memberof youbora.Infinity.prototype
    */
    unregister: function () {
      this._registeredProperties = null
      this._plugin.storage.removeLocal('inifnityRegisteredProperties')
    },

    /**
     * Splits params in dimensions (strings) and values (numbers)
     *
     * @param {Object} [params] Object of key:value params to split before adding to request.
     */
    _getParamsJson: function (dimensions, values, eventName, isNavigaton, isStart) {
      var returnparams = {}
      if (eventName) returnparams.name = eventName
      returnparams.dimensions = dimensions || {}
      returnparams.values = values || {}
      if (this._registeredProperties) {
        for (var key in this._registeredProperties.dimensions) {
          returnparams.dimensions[key] = this._registeredProperties.dimensions[key]
        }
        for (var key2 in this._registeredProperties.values) {
          returnparams.values[key2] = this._registeredProperties.values[key2]
        }
      }
      var paramsObject = { params: returnparams }
      if (isNavigaton) {
        if (paramsObject.params.dimensions.page) {
          paramsObject.params.page = paramsObject.params.dimensions.page
          delete paramsObject.params.dimensions.page
        }
        if (paramsObject.params.dimensions.route) {
          paramsObject.params.route = paramsObject.params.dimensions.route
          delete paramsObject.params.dimensions.route
        }
        if (!isStart) {
          delete paramsObject.params.dimensions
        }
        delete paramsObject.params.values
      }
      return paramsObject
    },

    isActive: function () {
      var now = new Date().getTime()
      if (this._plugin.storage.getLocal('infinityStarted') != null && !this._plugin.storage.getLocal('infinityStopped')) return true
      if (Number(this._plugin.getLastActive()) + this._plugin.sessionExpire > now) return true
      return false
    }
  },
  /** @lends youbora.Plugin */
  {
    // Static Memebers //
    /**
     * List of events that could be fired
     * @enum
     * @event
     */
    Event: {
      NAV: 'nav',
      SESSION_START: 'sessionStart',
      SESSION_STOP: 'sessionStop',
      BEAT: 'beat',
      EVENT: 'event'
    }
  }
)

module.exports = YouboraInfinity
