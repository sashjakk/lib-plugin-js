// {@see CdnParser}

module.exports = {
  cdnName: 'AKAMAI',
  requestMethod: 'GET',
  parsers: [{
    element: 'type+host',
    headerName: 'X-Cache',
    regex: /(.+)\sfrom.+AkamaiGHost\/(.+)\).+/
  }],
  requestHeaders: {
    Pragma: 'akamai-x-cache-on'
  },
  parseType: function (type) {
    if (type.indexOf('HIT') !== -1) {
      return 1
    } else if (type.indexOf('MISS') !== -1) {
      return 2
    }
    return 0
  }
}
