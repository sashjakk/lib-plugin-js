var YBRequest = require('../../request')
var Emitter = require('../../../emitter')
var Log = require('../../../log')

var DashParser = Emitter.extend(
  /** @lends youbora.DashParser.prototype */
  {
    /**
         * Class that asynchronously parses an DASH resource in order to get to the location URL.
         *
         * Since the CDN detection is performed with the resource url, it is essential that this
         * resource url is pointing to the CDN that is actually hosting the manifest.
         *
         * @constructs DashParser
         * @extends youbora.Emitter
         * @memberof youbora
         */
    constructor: function () {
      this._realResource = null
    },

    /**
         * Emits DONE event
         */
    done: function () {
      this.emit(DashParser.Event.DONE)
    },

    /**
         * Starts the Dash parsing from the given resource. The first (outside) call should set the
         * parentResource to null.
         *
         * @param {string} resource The resource url.
         */
    parse: function (resource) {
      var request = new YBRequest(resource, null, null, { cache: true })
      var locationRegexp = new RegExp(/.*<Location>(.+)<\/Location>.*/)
      var matches = null
      request.on(YBRequest.Event.SUCCESS, function (resp) {
        var manifest = resp.getXHR().responseText
        try {
          matches = locationRegexp.exec(manifest)
        } catch (err) {
          Log.warn('Dash location parse failed')
        }
        if (matches && matches[1]) {
          this.parse(matches[1])
        } else {
          this.parseFinalResource(manifest, resource)
        }
      }.bind(this))

      request.on(YBRequest.Event.ERROR, function (resp) {
        this.done()
      }.bind(this))

      request.send()
    },

    parseFinalResource: function (manifest, resource) {
      var segmentUrlRegexp = new RegExp(/[\s\S]*<SegmentURL[\s\S]*media="(.+)"[\s\S]*/)
      var segmentTemplateRegexp = new RegExp(/[\s\S]*<SegmentTemplate[\s\S]*media="(.+)"[\s\S]*/)
      var matches = null
      // first, segment url
      try {
        matches = segmentTemplateRegexp.exec(manifest)
      } catch (err) {
        Log.warn('Dash segment template parse failed')
      }
      // second, segment template
      if (matches === null) {
        try {
          matches = segmentUrlRegexp.exec(manifest)
        } catch (err) {
          Log.warn('Dash segment url parse failed')
        }
      }
      if (matches && matches[1] && this._isFullUrl(matches[1])) {
        if (matches[1].indexOf('"') > 0) {
          this._realResource = matches[1].substr(0, matches[1].indexOf('"'))
        } else {
          this._realResource = matches[1]
        }
      } else {
        this._realResource = resource
      }
      this.done()
    },

    _isFullUrl: function (url) {
      return url.indexOf('http') !== -1
    },

    /**
         * Get the parsed resource. Will be null/undefined if parsing is not yet started and can be a partial
         * (an intermediate manifest) result if the parser is still running.
         *
         * @return {string} The parsed resource.
         */
    getResource: function () {
      return this._realResource
    }
  },

  /** @lends youbora.DashParser */
  {
    // Static members

    /**
         * List of events that could be fired from this class.
         * @enum
         */
    Event: {
      /** Notifies that this DashParser is done processing. */
      DONE: 'done'
    }
  }
)

module.exports = DashParser
