/* global atv */
var Transform = require('./transform')
var HlsParser = require('./resourceparsers/hlsparser')
var CdnParser = require('./resourceparsers/cdnparser')
var DashParser = require('./resourceparsers/dashparser')
var LocationheaderParser = require('./resourceparsers/locationheaderparser')
var Log = require('../../log')
var Constants = require('../../constants')

var ResourceTransform = Transform.extend(
  /** @lends youbora.ResourceTransform.prototype */
  {
    /**
             * This class parses resource to fetch HLS transportstreams and CDN-related info.
             *
             * @constructs ResourceTransform
             * @extends youbora.Transform
             * @memberof youbora
             *
             * @param {Plugin} plugin Instance of {@link Plugin}
             */
    constructor: function (plugin) {
      ResourceTransform.__super__.constructor.apply(this, arguments)

      /** Defines if resourcetransform has finished working. */
      this.isDone = false

      this._plugin = plugin

      this._realResource = null
      this._initResource = null
      this._cdnName = null
      this._cdnNodeHost = null
      this._cdnNodeTypeString = null
      this._cdnNodeType = null
      this._responses = {}

      this._isBusy = false

      this.transformName = 'Resource'
    },

    /**
             * Get the resource. If the transform is done, the real (parsed) resource will be returned
             * Otherwise the initial one is returned.
             *
             * @return {string} The initial or parsed resource
             */
    getResource: function () {
      var ret = this._realResource
      if (this._plugin && this._plugin.options && this._plugin.options['content.resource'] && !this._plugin.options['parse.locationHeader'] && !this._plugin.options['parse.hls'] && !this._plugin.options['parse.dash']) {
        ret = this._plugin.options['content.resource']
      }
      return ret
    },

    /**
             * Get the parsed CDN name.
             *
             * @return {string} The CDN name or null if unknown
             */
    getCdnName: function () {
      return this._cdnName
    },

    /**
             * Get the parsed CDN node.
             *
             * @return {string} The CDN node or null if unknown
             */
    getNodeHost: function () {
      return this._cdnNodeHost
    },

    /**
             * Get the parsed CDN type string, as returned in the cdn header response.
             *
             * @return {string} The CDN type string
             */
    getNodeTypeString: function () {
      return this._cdnNodeTypeString
    },

    /**
             * Get the parsed CDN type, parsed from the type string.
             *
             * @return {string} The CDN type
             */
    getNodeType: function () {
      return this._cdnNodeType
    },

    /**
             * Start the execution. Can be called more than once. If already running, it will be ignored,
             * if ended it will restart.
             * @param {string} resource the original resource
             */
    init: function (resource) {
      if (!this._isBusy && !this.isDone) {
        this._isBusy = true

        this._initResource = resource

        this._hlsEnabled = this._plugin.isParseHls()
        this._cdnEnabled = this._plugin.isParseCdnNode()
        this._locHeaderEnabled = this._plugin.isLocHeader()
        this._dashEnabled = this._plugin.isDash()
        this._cdnList = this._plugin.getParseCdnNodeList().slice() // clone
        this._cdnNameHeader = this._plugin.getParseCdnNodeNameHeader()
        CdnParser.setBalancerHeaderName(this._cdnNameHeader)

        this._setTimeout()

        if (this._locHeaderEnabled) {
          this._parseLocHeader()
        }

        if (this._hlsEnabled) {
          this._parseHLS()
        } else if (this._dashEnabled) {
          this._dashParse()
        } else if (this._cdnEnabled) {
          this._parseCDN()
        } else {
          this.done()
        }
      }
    },

    done: function () {
      this.isDone = false
      Transform.prototype.done.apply(this, arguments) // super
    },

    _setTimeout: function () {
      // Abort operation after 3s

      try {
        this._parseTimeout = setTimeout(function () {
          if (this._isBusy) {
            this.done()
            Log.warn(
              'ResourceTransform has exceded the maximum execution time (3s) and will be aborted.'
            )
          }
        }.bind(this), 3000)
      } catch (err) {
        if (typeof atv !== 'undefined') {
          this._parseTimeout = atv.setTimeout(function () {
            if (this._isBusy) {
              this.done()
              Log.warn(
                'ResourceTransform has exceded the maximum execution time (3s) and will be aborted.'
              )
            }
          }.bind(this), 3000)
        } else {
          Log.error(err)
        }
      }
    },

    _parseLocHeader: function () {
      var headerParser = new LocationheaderParser()
      headerParser.parse(this._initResource)
      headerParser.on(LocationheaderParser.Event.DONE, function () {
        this._realResource = headerParser.getResource()
        this.done()
      }.bind(this))
    },

    _parseHLS: function () {
      var hlsTransform = new HlsParser()
      hlsTransform.on(HlsParser.Event.DONE, function () {
        this._realResource = hlsTransform.getResource()
        if (this._cdnEnabled) {
          this._parseCDN()
        } else {
          this.done()
        }
      }.bind(this))
      hlsTransform.parse(this._initResource)
    },

    _dashParse: function () {
      var dashParserTransform = new DashParser()
      dashParserTransform.on(DashParser.Event.DONE, function () {
        this._realResource = dashParserTransform.getResource()
        if (this._cdnEnabled) {
          this._parseCDN()
        } else {
          this.done()
        }
      }.bind(this))
      dashParserTransform.parse(this._initResource)
    },

    _parseCDN: function () {
      if (this._cdnList.length > 0) {
        var cdn = this._cdnList.shift()

        if (this.getNodeHost()) return // abort

        var cdnParser = CdnParser.create(cdn)

        if (cdnParser) {
          cdnParser.on(CdnParser.Event.DONE, function () {
            this._responses = cdnParser.getResponses()
            this._cdnName = cdnParser.getParsedCdnName()
            this._cdnNodeHost = cdnParser.getParsedNodeHost()
            this._cdnNodeTypeString = cdnParser.getParsedNodeTypeString()
            this._cdnNodeType = cdnParser.getParsedNodeType()

            if (this.getNodeHost()) {
              this.done()
            } else {
              this._parseCDN()
            }
          }.bind(this))

          // Run parse
          cdnParser.parse(this.getResource(), this._responses)
        } else {
          this._parseCDN()
        }
      } else {
        this.done()
      }
    },

    /**
             * Replaces the resource and/or Cdn info for the /start service.
             *
             * @param {YBRequest} request YBRequest to transform.
             */
    parse: function (request) {
      if (request.service === Constants.Service.START) {
        var lastSent = this._plugin.requestBuilder.lastSent
        lastSent.parsedResource = request.params.parsedResource = this.getResource()

        if (this._cdnEnabled) {
          lastSent.cdn = request.params.cdn = request.params.cdn || this.getCdnName()
          lastSent.nodeHost = request.params.nodeHost = this.getNodeHost()
          lastSent.nodeType = request.params.nodeType = this.getNodeType()
          lastSent.nodeTypeString = request.params.nodeTypeString = this.getNodeTypeString()
        }
      }
    }
  })

module.exports = ResourceTransform
