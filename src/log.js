var Emitter = require('./emitter')
var isArray = require('./mixins/isarray')

/**
 * Static Log class for YouboraLib
 *
 * @class
 * @static
 * @memberof youbora
 */
var Log = {

  _emitter: new Emitter(),

  /** Exposes {@link youbora.EvenfulObject.on} */
  on: function () { Log._emitter.on.apply(Log._emitter, arguments) },
  /** Exposes {@link youbora.EvenfulObject.off} */
  off: function () { Log._emitter.off.apply(Log._emitter, arguments) },
  /** Exposes {@link youbora.EvenfulObject.emit} */
  emit: function () { Log._emitter.emit.apply(Log._emitter, arguments) },

  /**
   * Enum for log levels
   * @enum
   */
  Level: {
    /** No console outputs */
    SILENT: 6,
    /** Console will show errors */
    ERROR: 5,
    /** Console will show warnings */
    WARNING: 4,
    /** Console will show notices (ie: life-cyrcle logs) */
    NOTICE: 3,
    /** Console will show debug messages (ie: player events) */
    DEBUG: 2,
    /** Console will show verbose messages (ie: Http Requests) */
    VERBOSE: 1
  },

  /**
   * Enum for events
   * @enum
   */
  Event: {
    /** Sent each time a messaged is issued, even if log level does not handle it. */
    LOG: 'log'
  },

  /**
   * Only logs of this imporance or higher will be shown.
   * @default youbora.Log.Levels.ERROR
   * @see {@link youbora.Log.Levels}
   */
  logLevel: 5,

  /**
   * If true, console logs will always be outputed without colors (for debbugin in devices).
   * @default false
   */
  plainLogs: false,

  /**
   * Returns a console message
   *
   * @private
   * @param {(string|error|array)} msg Message string, error object or array of messages.
   * @param {Log.Levels} [level=Log.Levels.NOTICE] Defines the level of the error sent.
   * Only errors with higher or equal level than Log.logLevel will be displayed.
   * @param {string} [color=darkcyan] Color of the header
   * @see {@link Youbora.Log.debugLevel}
   */
  report: function (msg, level, color) {
    if (typeof console !== 'undefined' && console.log && typeof document !== 'undefined') {
      level = level || Log.Level.NOTICE
      color = color || 'darkcyan'

      var letters = {
        5: 'e', // Error
        4: 'w', // Warning
        3: 'n', // Notice
        2: 'd', // Debug
        1: 'v' // Verbose
      }

      var letter = letters[level]
      var prefix = '[Youbora]' + Log._getCurrentTime() + ' ' + letter + ':'

      this.emit('log', { level: level, msg: msg, prefix: prefix })

      // Show messages in actual console if level is enought
      if (Log.logLevel <= level) {
        if (Log.plainLogs || document.documentMode) { // document.documentMode exits only in IE
          // Plain log for IE and devices
          Log._plainReport(msg, prefix)
        } else {
          // choose log method
          var logMethod
          if (level === Log.Level.ERROR && console.error) {
            logMethod = console.error
          } else if (level === Log.Level.WARNING && console.warn) {
            logMethod = console.warn
          } else if (level === Log.Level.DEBUG && console.debug) {
            logMethod = console.debug
          } else {
            logMethod = console.log
          }

          // print message
          prefix = '%c' + prefix
          if (isArray(msg)) {
            msg.splice(0, 0, prefix, 'color: ' + color)
            logMethod.apply(console, msg)
          } else {
            logMethod.call(console, prefix, 'color: ' + color, msg)
          }
        }
      }
    }
  },

  /**
   * Returns the current time in format hh:mm:ss.mmm (with trailing 0s)
   * @private
   * @return {string} Current time.
   */
  _getCurrentTime: function () {
    var d = new Date()
    var hh = ('0' + d.getDate()).slice(-2)
    var mm = ('0' + d.getMinutes()).slice(-2)
    var ss = ('0' + d.getSeconds()).slice(-2)
    var mmm = ('00' + d.getMilliseconds()).slice(-3)
    return '[' + hh + ':' + mm + ':' + ss + '.' + mmm + ']'
  },

  /**
   * Returns a console message without style
   *
   * @private
   * @param {(string|object|array)} msg Message string, object or array of messages.
   * @param {string} prefix Prefix of the message.
   */
  _plainReport: function (msg, prefix) {
    if (msg instanceof Array) {
      for (var m in msg) {
        Log._plainReport(msg[m], prefix)
      }
    } else {
      if (typeof msg === 'string') {
        console.log(prefix + ' ' + msg)
      } else {
        console.log(prefix + ' <next line>')
        console.log(msg)
      }
    }
  },

  /**
   * Sends an error (level 1) console log.
   * Supports unlimited arguments: ("this", "is", "a", "message")
   * @memberof $YB
   * @see {@link $YB.report}
   */
  error: function () {
    Log.report([].slice.call(arguments), Log.Level.ERROR, 'darkred')
  },

  /**
   * Sends a warning (level 2) console log.
   * Supports unlimited arguments: ("this", "is", "a", "message")
   * @memberof $YB
   * @see {@link $YB.report}
   */
  warn: function () {
    Log.report([].slice.call(arguments), Log.Level.WARNING, 'darkorange')
  },

  /**
   * Sends a notice (level 3) console log.
   * Supports unlimited arguments: ("this", "is", "a", "message")
   * @memberof $YB
   * @see {@link $YB.report}
   */
  notice: function () {
    Log.report([].slice.call(arguments), Log.Level.NOTICE, 'darkgreen')
  },

  /**
   * Sends a debug message (level 4) to console.
   * Supports unlimited arguments: ("this", "is", "a", "message")
   * @memberof $YB
   * @see {@link $YB.report}
   */
  debug: function () {
    Log.report([].slice.call(arguments), Log.Level.DEBUG, 'indigo')
  },

  /**
   * Sends a verbose message (level 5) to console.
   * Supports unlimited arguments: ("this", "is", "a", "message")
   * @memberof $YB
   * @see {@link $YB.report}
   */
  verbose: function () {
    Log.report([].slice.call(arguments), Log.Level.VERBOSE, 'navy')
  },

  /**
   * This function is automatically executed at youboralib's init.
   * Will search inside window.location.search for attribute 'youbora-debug=X'.
   * X can have one of these values, that will modify LogLevel.
   * 6: SILENT,
   * 5: ERROR,
   * 4: WARNING,
   * 3: NOTICE,
   * 2: DEBUG,
   * 1: VERBOSE
   *
   * If youbora-console=plain is present, plainLogs will be set to true.
   */
  loadLevelFromUrl: function () {
    if (typeof window !== 'undefined' && window.location && window.location.search) {
      var m = /\?.*&*youbora-debug=(.+)/i.exec(window.location.search)
      if (m !== null) {
        Log.logLevel = m[1]
      }

      var m2 = /\?.*&*youbora-debug=plain/i.exec(window.location.search)
      if (m2 !== null) {
        Log.plainLogs = true
      }
    }
  }
}

module.exports = Log
